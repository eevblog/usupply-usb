set(ENABLE_CLANG_TIDY ON CACHE BOOL "Add clang-tidy automatically to builds")
if (ENABLE_CLANG_TIDY)
        find_program(CLANG_TIDY_EXE NAMES "C:\\Program Files\\LLVM\\bin\\clang-tidy.exe")
        if (CLANG_TIDY_EXE)
                message(STATUS "clang-tidy found: ${CLANG_TIDY_EXE}")
                set(CLANG_TIDY_CHECKS "-*,modernize-*")
                set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXE};-checks=${CLANG_TIDY_CHECKS};-header-filter='${CMAKE_SOURCE_DIR}/*'"
                CACHE STRING "" FORCE)
        else()
                message(AUTHOR_WARNING "clang-tidy not found!")
                set(CMAKE_CXX_CLANG_TIDY "" CACHE STRING "" FORCE) # delete it
        endif()
endif()