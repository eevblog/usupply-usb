**Rules for contributers:**
- No exceptions are allowed.
- No reliance on RTTI is allowed.
- No allocation allowed (prevents optimisations in some cases).
- Template code is permitted.

**Remember:**
- std::function, std::string, std::vector, std::list, std::deque, std::queue use allocation.
- noexcept does not garentee exceptions are not thrown, only that std::terminate will be called if an exception is thrown
- This is an embedded target try to keep EBO possible when creating classes that might have empty bases.

**VS Code extensions:**
- C/C++
- CMake
- CMake Tools
- CMake Tools Helper
- Cortex-Debug (To use this you need to add the path to openocd)
- LinkerScript
- ${platform}-arm-none-eabi

1: For cmake to work you need to set the path to cmake in visual studio code.

**Required tools:**
- Visual Studio Code (or you can manually use raw CMAKE) (https://code.visualstudio.com/)
- gcc-arm-none-eabi (https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads)
- binutils-arm-none-eabi (https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads)
- OpenOCD (https://github.com/gnu-mcu-eclipse/openocd/releases)
- Ninja (https://github.com/ninja-build/ninja)

1: For OpenOCD, Ninja, BinUtils and GCC to work you need to set the path to cmake in visual studio code.

**Recommended tools:**
- Map File Browser (http://www.sikorskiy.net/prj/amap/index.html)