#pragma once

namespace General
{
	/**
	 * @brief This is a helper class which returns the argument at the index i.
	 * 
	 * @tparam i The index to return.
	 * @tparam Args The argument types.
	 */
	template <unsigned i, typename ... Args>
	struct GetAtIndexHelper;
	template <typename Head, typename ... Tail>
	struct GetAtIndexHelper<0, Head, Tail ...>
	{
		constexpr static decltype(auto) Run(Head && head, Tail && ...) noexcept
		{
			return std::forward<Head>(head);
		}
	};
	template <typename Head>
	struct GetAtIndexHelper<0, Head>
	{
		constexpr static decltype(auto) Run(Head && head) noexcept
		{
			return std::forward<Head>(head);
		}
	};
	template <unsigned i, typename Head, typename ... Tail>
	struct GetAtIndexHelper<i, Head, Tail...>
	{
		constexpr static decltype(auto) Run(Head &&, Tail && ... tail) noexcept
		{
			return GetAtIndexHelper<i - 1, Tail...>::Run(std::forward<Tail>(tail)...);
		}
	};
	/**
	 * @brief Takes a list of forwarded arguemnts and returns the arguement at the requested index.
	 * 
	 * @tparam i The argument index to return.
	 * @tparam Args The type of the arguments, where the first argument is index 0.
	 * @param args The aguments, where the first argument is index 0.
	 * @return decltype(auto) A deduced return type from the above specified index.
	 */
	template <unsigned i, typename ... Args>
	constexpr decltype(auto) ArgAtIndex(Args && ... input) noexcept
	{
		static_assert(i < sizeof...(Args), "Cannot get an index that is beyond the number of parameters.");
		return GetAtIndexHelper<i, Args...>::Run( std::forward<Args>(input) ... );
	}
}