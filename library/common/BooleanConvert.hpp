#pragma once

#include "ArrayConvert.hpp"
#include "Char.hpp"
#include "FIFO.hpp"
#include "Math.hpp"

namespace General
{
	/**
	 * @brief Convert a boolean value to ascii.
	 * 
	 * @tparam fixed_length The index for characters to output.
	 * @tparam N The capcity of the output array.
	 * @tparam L The length of the True label.
	 * @tparam M The length of the False label.
	 * @param input The boolean to convert.
	 * @param output The place to print the bool.
	 * @param true_value The string to print for true.
	 * @param false_value The string to print for false.
	 * @param offset The offset in the output array to start printing.
	 * @param padding The character to place when padding is required.
	 * @return std::size_t The number of characters inserted into the output.
	 */
	template <std::size_t fixed_length, std::size_t N, std::size_t L, std::size_t M>
	std::size_t constexpr fixed_btoa( bool input, std::array<char, N> &output, std::array<char, L> const &true_value, std::array<char, M> const &false_value, std::size_t offset = 0, char padding = ' ' ) noexcept
	{
		static_assert( fixed_length <= N, "Fixed length cannot be larger than input buffer." );

		using container = std::array<char, fixed_length>;
		auto & out = (container &)output;

		auto i = 0u;

		//Fill leading padding
		for ( ; i < offset; ++i )
			out[ i ] = padding;

		std::size_t minimum = 0u;
		if ( input )
		{
			//true
			minimum = Minimum( fixed_length, L + offset );
			for ( ; i < minimum; ++i )
				out[ i ] = true_value[ i - offset ];
		}
		else
		{
			//false
			minimum = Minimum( fixed_length, M + offset );
			for ( ; i < minimum; ++i )
				out[ i ] = false_value[ i - offset ];
		}

		//Remove trailing null if it exists
		if ( i > 0 )
			if ( out[ i - 1 ] == '\0' )
				--i;

		//Fill trailing padding
		for ( ; i < fixed_length; ++i )
			out[ i ] = padding;

		return fixed_length;
	}
	/**
	 * @brief Converts a character array to a bool.
	 * 
	 * @tparam N The number of characters in the input array.
	 * @tparam L The length of the true value.
	 * @tparam M The length of the false value.
	 * @tparam bool The type of the result of the conversion.
	 * @param input The input array.
	 * @param output The outut boolean type (or similar type).
	 * @param true_value The true string to match.
	 * @param false_value The false string to match.
	 * @param offset The offset to start scanning the input array.
	 * @return std::size_t Number of characters that were parsed.
	 */
	template <std::size_t N, std::size_t L, std::size_t M, typename T = bool>
	std::size_t constexpr atob( std::array<char, N> const &input, T &output, std::array<char, L> const &true_value, std::array<char, M> const &false_value, std::size_t offset = 0 ) noexcept
	{
		auto const smaller_true  = L;
		auto const smaller_false = M;

		for ( auto i = 0u; i < smaller_true; ++i )
		{
			auto const current = true_value[ i ];
			auto const index   = i + offset;
			if ( index < N )
			{
				if ( Equivalent( input[ index ], current ) )
				{
					if ( i + 1 == smaller_true )
					{
						output = true;
						return smaller_true;
					}
				}
				else if ( current == '\0' )
				{
					output = true;
					return smaller_true;
				}
				else
					break;
			}
			else
				return 0;
		}

		for ( auto i = 0u; i < smaller_false; ++i )
		{
			auto const current = false_value[ i ];
			auto const index   = i + offset;
			if ( index < N )
			{
				if ( Equivalent( input[ index ], current ) )
				{
					if ( i + 1 == smaller_false )
					{
						output = false;
						return smaller_false;
					}
				}
				else if ( current == '\0' )
				{
					output = false;
					return smaller_false;
				}
				else
					break;
			}
			else
				return 0;
		}
		return 0;
	}
}