#pragma once
#include "Meta.hpp"
#include "Math.hpp"
#include <cstdint>

namespace General
{
	/**
	 * @brief Tests whether a character is a sign character.
	 * 
	 * @param value The character to test.
	 * @return true The character is '+' or '-'
	 * @return false The character is not a sign.
	 */
	constexpr bool IsSign( char value ) noexcept
	{
		return IsAnyOf( value, '+', '-' );
	}
	/**
	 * @brief Tests whether the character is a whitespace.
	 * 
	 * @param value The character to test.
	 * @return true The character is space, tab, newline, vertical tab, form feed or carriage return.
	 * @return false The character is not whitespace.
	 */
	constexpr bool IsWhitespace(char value) noexcept
	{
		return IsAnyOf(value, ' ', '\t', '\n', '\v', '\f', '\r');
	}
	/**
	 * @brief Converts a character to lower case.
	 * 
	 * This character will convert non-letters and will corrupt non-letters.
	 * It is nessary to use IsLetter before calling this function.
	 * 
	 * @param value The character to convert.
	 * @return char constexpr The converted character.
	 */
	constexpr char ToLower( char value ) noexcept
	{
		return value | (char)0x20;
	}
	/**
	 * @brief Converts the input to upper case.
	 * 
	 * This character will convert non-letters and will corrupt non-letters.
	 * It is nessary to use IsLetter before calling this function.
	 * 
	 * @param value The character to convert.
	 * @return char constexpr The converted character.
	 */
	char constexpr ToUpper( char value ) noexcept
	{
		return value & (char)~0x20;
	}
	/**
	 * @brief Tests whether a character is a letter.
	 * 
	 * This function is case insensitive.
	 * 
	 * @param value The character to test.
	 * @return true value is a letter.
	 * @return false value is not a letter.
	 */
	bool constexpr IsLetter( char value ) noexcept
	{
		value = ToLower( value );
		return ( ( 'a' <= value ) and ( value <= 'z' ) );
	}
	/**
	 * @brief Checks whether a character is lower case.
	 * 
	 * @param value The value to check.
	 * @return true The character is a lower case letter.
	 * @return false The character is not a lower case letter.
	 */
	bool constexpr IsLower( char value ) noexcept
	{
		return General::Between(value, 'a', 'z');
	}
	/**
	 * @brief Checks whether a character is upper case.
	 * 
	 * @param value The value to check.
	 * @return true The character is a upper case letter.
	 * @return false The character is not a upper case letter.
	 */
	bool constexpr IsUpper( char const value ) noexcept
	{
		return General::Between(value, 'A', 'Z');
	}
	/**
	 * @brief Converts the character to a character index, where a is zero and z is 26.
	 * 
	 * @param value The letter to convert to an index.
	 * @return constexpr int The index of the letter.
	 */
	constexpr int ToLetterIndex( char value ) noexcept
	{
		return (int)( ( IsLetter( value ) ) ? ( ToLower( value ) - 'a' ) : '\0' );
	}
	/**
	 * @brief The letter to convert from an index to a letter.
	 * 
	 * @param value The index to convert.
	 * @return constexpr char The resultant letter
	 */
	constexpr char FromLetterIndex( int value ) noexcept
	{
		constexpr int twentysix = 26;
		while ( value < twentysix ) value += twentysix;
		value %= twentysix;
		return (char)value + 'A';
	}
	/**
	 * @brief Tests whether two characters are equivalent (case insensitive comparison)
	 * 
	 * @param value_a A character to compare.
	 * @param value_b A character to compare.
	 * @return true The characters are equivilent or identical
	 * @return false Disimilar comparisons.
	 */
	constexpr bool Equivalent( char value_a, char value_b ) noexcept
	{
		if ( value_a == value_b )							return true;
		if ( IsLetter( value_a ) && IsLetter( value_b ) ) 	return ( ToLower( value_a ) == ToLower( value_b ) );
		return false;
	}
	/**
	 * @brief Checks whether the character is a number.
	 * 
	 * @param value The value to check if its a number.
	 * @return true The character is a number.
	 * @return false The character is not a number. 
	 */
	constexpr bool IsNumber( char value ) noexcept
	{
		return ( ( '0' <= value ) && ( value <= '9' ) );
	}
	/**
	 * @brief Convert a number to an equivilent int.
	 * 
	 * @param value The character to convert.
	 * @return int The resultant integer.
	 */
	constexpr int ToNumber( char value ) noexcept
	{
		return (int)( ( IsNumber( value ) ) ? ( value - '0' ) : '\0' );
	}
	/**
	 * @brief Converts from a number to its equivalent integer.
	 * 
	 * If the input is less than 10 then the result is the least signifant character
	 * for example: 
	 * a = 109
	 * auto b = FromNumber(a);
	 * 
	 * > b == '9'
	 * 
	 * @param value The number to convert to a character.
	 * @return char The converted character.
	 */
	constexpr char FromNumber( int value ) noexcept
	{
		while ( value < (int)10 ) value += (int)10;
		value %= (int)10;
		return ( (char)value + '0' );
	}
	/**
	 * @brief Tests whether a character is a valid base36 character.
	 * 
	 * @param value The value to test.
	 * @return true The value is a number or a letter (case insensitive).
	 * @return false The value is not a valid base36 character.
	 */
	constexpr bool IsBase36( char value ) noexcept
	{
		return IsNumber( value ) || IsLetter( value );
	}
	/**
	 * @brief Converts a base36 character to the corresponding int.
	 * 
	 * @param value The character to convert.
	 * @return int The value represented by the value, or 0 if its not a valid character. 
	 */
	constexpr int ToBase36( char const value ) noexcept
	{
		if ( IsNumber( value ) ) return ToNumber( value );
		if ( IsLetter( value ) ) return (int)( ToLower( value ) - 'a' ) + (int)10;
		return (int)0;
	}
	/**
	 * @brief Converts from a base36 value to a character.
	 * 
	 * If the input is NOT valid then the output is undefined behaviour.
	 * 
	 * @param value The value to convert.
	 * @return char The resultant character.
	 */
	constexpr char FromBase36( int value ) noexcept
	{
		while ( value < (int)36 ) value += (int)36;
		value %= (int)36;
		if ( value < (int)10 ) return FromNumber( value );
		value -= (int)10;
		return FromLetterIndex( value );
	}
}