#pragma once
#include "Meta.hpp"
#include <type_traits>
#include <utility>

namespace Containers
{
	/**
	 * @brief A ping pong buffer that is used to swap quickly between two buffers.
	 * 
	 * This can be used for DMA transfers and allows one buffer to transmit while another is loaded.
	 * 
	 * @tparam Container The underlying container for the buffers (the two buffers)
	 */
	template <typename Container>
	class PingPong 
	{
	private:
		using T = typename Container::value_type;
		//
		Container		m_Storage[2]{};
		unsigned		m_Current{0};
	public:
		/**
		 * @brief Construct the underlying containers in the buffer from the same input arguments.
		 * 
		 * @tparam Args The input argument types.
		 * @param args The input arguments.
		 */
		template <typename ... Args>
		constexpr PingPong(Args && ... args) noexcept :
			m_Storage	
			{
				{ std::forward<Args>(args)... }, 
				{ std::forward<Args>(args)... }
			}
		{}
		/**
		 * @brief The buffer that is being operated on in the background.
		 * 
		 * This is the actively loaded or unloaded half of the ping pong buffer.
		 * 
		 * @return constexpr Container& 
		 */
		constexpr Container & Current() noexcept
		{
			return m_Storage[ m_Current ];
		}
		/**
		 * @brief The buffer that is being operated on in the background.
		 * 
		 * This is the actively loaded or unloaded half of the ping pong buffer.
		 * 
		 * @return constexpr Container& 
		 */
		constexpr Container const & Current() const noexcept
		{
			return m_Storage[ m_Current ];
		}
		/**
		 * @brief The buffer that is being processed by software.
		 * 
		 * @return constexpr Container& 
		 */
		constexpr Container & Other() noexcept
		{
			return m_Storage[ m_Current ];
		}
		/**
		 * @brief The buffer that is being processed by software.
		 * 
		 * @return constexpr Container const & 
		 */
		constexpr Container const & Other() const noexcept
		{
			return m_Storage[ m_Current ];
		}
		/**
		 * @brief Swap the buffers and clear the other buffer.
		 */
		constexpr void Submit() noexcept
		{
			Other().Clear();
			m_Current ^= 1u;
		}
	};
}