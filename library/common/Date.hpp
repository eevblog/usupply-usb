#pragma once

#include "IntegerConvert.hpp"
#include <cassert>

namespace General
{
	struct DateYear;
	struct DateMonth;
	struct DateDay;
	/**
	 * @brief An enum of the avaliable weekdays.
	 */
	enum class Weekday : std::int8_t
	{
		Monday    = 1,
		Tuesday   = 2,
		Wednesday = 3,
		Thursday  = 4,
		Friday    = 5,
		Saturday  = 6,
		Sunday    = 7,
		Today
	};
	/**
	 * @brief An enum of the avalaibe months
	 */
	enum class Month : std::int8_t
	{
		January   = 1,
		February  = 2,
		March     = 3,
		April     = 4,
		May       = 5,
		June      = 6,
		July      = 7,
		August    = 8,
		September = 9,
		October   = 10,
		November  = 11,
		December  = 12,
	};
	/**
	 * @brief An enum for the date order.
	 */
	enum class DateOrder
	{
		YMD,
		DMY,
		MDY
	};
	/**
	 * @brief A data structure that container day, month and year.
	 * 
	 */
	class Date
	{
	protected:
		std::int8_t m_Year, m_Month, m_Day;

	public:
		/**
		 * @brief Construct a new Date object
		 * 
		 * @param year The year.
		 * @param month The month.
		 * @param day The day.
		 */
		Date( std::int8_t const &year, std::int8_t const &month, std::int8_t const &day ) noexcept:
		    m_Year( year ),
		    m_Month( month ),
		    m_Day( day )
		{}
		/**
		 * @brief Returns a DateYear object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateYear& The Date object
		 */
		DateYear & Year() noexcept
		{
			return static_cast<DateYear &>( *this );
		}
		/**
		 * @brief Returns a DateMonth object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateMonth& The Date object
		 */
		DateMonth & Month() noexcept
		{
			return static_cast<DateMonth &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateDay & Day() noexcept
		{
			return static_cast<DateDay &>( *this );
		}
		/**
		 * @brief Returns a DateYear object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateYear& The Date object
		 */
		DateYear const & Year() noexcept const
		{
			return static_cast<DateYear &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateMonth const & Month() noexcept const
		{
			return static_cast<DateMonth &>( *this );
		}
		/**
		 * @brief Returns a DateDay object, which inherits from Date.
		 * 
		 * The result has the same underlying storage as the Date object.
		 * 
		 * @return DateDay& The Date object
		 */
		DateDay const & Day() noexcept const
		{
			return static_cast<DateDay &>( *this );
		}
		/**
		 * @brief Converts the date to a string, which is returned as a constant sized string.
		 * 
		 * @tparam DateOrder::YMD The order to output the date.
		 * @tparam NullTerminated Whether to ensure a null termination at the end of the string.
		 * @return auto The output string (in the form of a character array)
		 */
		template <DateOrder Order = DateOrder::YMD, bool NullTerminated = false>
		auto ToString() noexcept const
		{
			static constexpr auto L = 2u;
			static constexpr auto N = 8u + (unsigned)NullTerminated;
			static constexpr auto S = '/';
			std::array<char, N> output{};
			{
				auto length = 0u;
				if constexpr ( Order == DateOrder::YMD )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
				}
				if constexpr ( Order == DateOrder::MDY )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
				}
				if constexpr ( Order == DateOrder::DMY )
				{
					length += General::fixed_utoa<L>( (std::int8_t)m_Day, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Month, output, length, '0' );
					if ( length < N ) output[ length++ ] = S;
					length += General::fixed_utoa<L>( (std::int8_t)m_Year, output, length, '0' );
				}
				if constexpr ( NullTerminated )
					output[ N - 1 ] = '\0';
			}
			return output;
		}
	};
	/**
	 * @brief An object that deals with an assignable year object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct DateYear : Date
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear( const DateYear & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateYear &operator=( const DateYear & ) = delete;
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param value The value to assign.
		 * @return Date& The underlying date object.
		 */
		Date &operator=( std::int8_t value ) noexcept
		{
			m_Year = value;
			return *this;
		}
		/**
		 * @brief Implicity converts to the year as a int8_t (2 digit year)
		 * 
		 * @return std::int8_t The resulting year.
		 */
		operator std::int8_t() const
		{
			return m_Year;
		}
	};
	/**
	 * @brief An object that deals with an assignable month object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct DateMonth : Date
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth( const DateMonth & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateMonth &operator=( const DateMonth & ) = delete;
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param value The value to assign.
		 * @return Date& The underlying date object.
		 */
		Date &operator=( General::Month const &value ) noexcept
		{
			m_Month = (std::int8_t)value;
			return *this;
		}
		/**
		 * @brief Converts the month to a month enum.
		 * 
		 * @return General::Month The month as an enum.
		 */
		operator General::Month() noexcept const
		{
			return (General::Month)m_Month;
		}
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param input The value to assign.
		 * @return Date& The underlying date object.
		 */
		Date &operator=( std::int8_t input ) noexcept
		{
			m_Month = (std::int8_t)input;
			return *this;
		}
		/**
		 * @brief Implicitly converts the Month to a int8_t.
		 * 
		 * @return int8_t The month as a int8_t where January = 1.
		 */
		operator int8_t() noexcept const
		{
			return m_Month;
		}
	};
	/**
	 * @brief An object that deals with an assignable day object that supports chained date operations.
	 * 
	 * The eventual purpose of this is to provide to and from string capibilities.
	 */
	struct DateDay : Date
	{
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay() = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay( const DateDay & ) = delete;
		/**
		 * @brief Deleted this class can only be constructed from a Date object.
		 */
		constexpr DateDay &operator=( const DateDay & ) = delete;
		/**
		 * @brief Assign a weekday to the date.
		 * 
		 * @param input The day to assign
		 * @return Date& The underlying date object.
		 */
		Date &operator=( Weekday input ) noexcept
		{
			m_Month = (std::int8_t)input;
			return *this;
		}
		/**
		 * @brief Converts the day to a Weekday enum.
		 * 
		 * @return Weekday The weekday as an enum.
		 */
		operator Weekday() noexcept const
		{
			return (Weekday)m_Day;
		}
		/**
		 * @brief Assigns a value to the component of the the underlying date object
		 * 
		 * @param input The value to assign.
		 * @return Date& The underlying date object.
		 */
		Date &operator=( std::int8_t const &input ) noexcept
		{
			m_Day = input;
			return *this;
		}
		/**
		 * @brief Implicitly converts the Day to a int8_t.
		 * 
		 * @return int8_t The month as a int8_t where Monday = 1.
		 */
		operator std::int8_t() noexcept const
		{
			return m_Day;
		}
	};
}