#pragma once

#include <cstdint>

namespace General
{
	/**
	 * @brief An enum for the basic integer type.
	 */
	enum IntegerType : unsigned
	{
		Unsigned = 0u,
		Signed   = 1u
	};
	/**
	 * @brief A utility class to access integers and equivilent integer types in a metaprogramming context.
	 * 
	 * @todo Convert some of this to use std::numeric_limits.
	 * 
	 * @tparam Type whether the integer is signed or unsigned.
	 * @tparam Bits The number of bits in the integer.
	 */
	template <IntegerType Type, unsigned Bits>
	struct Integer
	{
		static constexpr unsigned next_boundary()
		{
			constexpr unsigned boundaries[] = { 8, 16, 32, 64 };
			for ( unsigned index = 0u; index < 4u; ++index )
				if ( boundaries[ index ] >= Bits )
					return boundaries[ index ];
			return 0;
		}

		static constexpr unsigned bit_boundary = next_boundary();
		static constexpr unsigned padding      = bit_boundary - Bits;
		static_assert( bit_boundary > 0, "Cannot store more than 64 bits in an integer." );

		using type = typename Integer<Type, bit_boundary>::type;

		static constexpr type base_mask  = ~((type)0u);
		static constexpr type ShiftRight = ( padding + Type );
		static constexpr type max        = base_mask >> ShiftRight;
	};

	template <>
	struct Integer<Signed, 8u>
	{
		static constexpr unsigned bits = 8u;
		using type                     = std::int8_t;
		static constexpr type max      = 0x7fu;
	};
	template <>
	struct Integer<Signed, 16u>
	{
		static constexpr unsigned bits = 16u;
		using type                     = std::int16_t;
		static constexpr type max      = 0x7fffu;
	};
	template <>
	struct Integer<Signed, 32u>
	{
		static constexpr unsigned bits = 32u;
		using type                     = std::int32_t;
		static constexpr type max      = 0x7fffffffu;
	};
	template <>
	struct Integer<Signed, 64u>
	{
		static constexpr unsigned bits = 64u;
		using type                     = std::int64_t;
		static constexpr type max      = 0x7fffffffffffffffu;
	};
	template <>
	struct Integer<Unsigned, 8u>
	{
		static constexpr unsigned bits = 8u;
		using type                     = std::uint8_t;
		static constexpr type max      = 0xffu;
	};
	template <>
	struct Integer<Unsigned, 16u>
	{
		static constexpr unsigned bits = 16u;
		using type                     = std::uint16_t;
		static constexpr type max      = 0xffffu;
	};
	template <>
	struct Integer<Unsigned, 32u>
	{
		static constexpr unsigned bits = 32u;
		using type                     = std::uint32_t;
		static constexpr type max      = 0xffffffffu;
	};
	template <>
	struct Integer<Unsigned, 64u>
	{
		static constexpr unsigned bits = 64u;
		using type                     = std::uint64_t;
		static constexpr type max      = 0xffffffffffffffffu;
	};
	/**
	 * @brief Returns an alias to the Integer class which is unsigned.
	 * 
	 * @tparam bits The number of bits in the integer.
	 */
	template <unsigned bits>
	using UnsignedInt = Integer<Unsigned, bits>;
	/**
	 * @brief Returns an alias to the Integer class which is signed.
	 * 
	 * @tparam bits The number of bits in the integer.
	 */
	template <unsigned bits>
	using SignedInt = Integer<Signed, bits>;
	/**
	 * @brief Returns an alias to the Integer type which is unsigned.
	 * 
	 * @tparam bits The number of bits in the integer.
	 */
	template <unsigned bits>
	using UnsignedInt_t = typename Integer<Unsigned, bits>::type;
	/**
	 * @brief Returns an alias to the Integer type which is signed.
	 * 
	 * @tparam bits The number of bits in the integer.
	 */
	template <unsigned bits>
	using SignedInt_t = typename Integer<Signed, bits>::type;
	/**
	 * @brief Returns an integer with the same number of bits as the input and is signed.
	 * 
	 * @param T The type which will be matched in size.
	 */
	template <typename T>
	using SignedVersion_t = SignedInt_t<8u * sizeof( T )>;
	/**
	 * @brief Returns an integer with the same number of bits as the input and is unsigned.
	 * 
	 * @param T The type which will be matched in size.
	 */
	template <typename T>
	using UnsignedVersion_t = UnsignedInt_t<8u * sizeof( T )>;
}