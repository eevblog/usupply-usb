#pragma once

#include "AtIndex.hpp"
#include "Segment.hpp"
#include <cstdint>

namespace LCD::Block
{
	/**
	 * @brief A class that manages the segments on an LCD to display a bargraph.
	 * 
	 * @tparam base_class The LCD class that contains all the segments.
	 * @tparam bars The segments that contain the bargraph elements (in order).
	 */
	template <typename base_class, typename ... bars>
	struct Bargraph : public base_class
	{
		/**
		 * @brief A list of the bargraph segemnts.
		 * 
		 */
		using list_t = General::TypeList<bars...>;
		/**
		 * @brief The number of bargraph segments
		 * 
		 */
		static constexpr auto count = sizeof...(bars);
		/**
		 * @brief Get a bargraph element at a requested index.
		 * 
		 * @tparam i The index.
		 */
		template <unsigned i>
		using AtIndex_t = typename General::template AtIndex<i, list_t>::type;
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr Bargraph() = delete;
		constexpr Bargraph(const Bargraph&) = delete;
		constexpr Bargraph& operator=(const Bargraph&) = delete;
		/**
		 * @brief Set the value of the bargraph to active up to the index specified in the input.
		 * 
		 * @tparam c The working index.
		 * @param input The index to display up to.
		 * @return true The assignment was completed.
		 * @return false There was an error with input parameters.
		 */
		template <unsigned c>
		bool Set(unsigned input) noexcept
		{
			if constexpr (c == 0)
				return false;
			else
			{
				auto & this_v = (*this);
				auto & segment = ((AtIndex_t<count - c> &)this_v);
				segment.Assign(input >= c);

				if constexpr (c == 1)
					return true;
				else
					return Set<c - 1>(input);
			}
		}
		/**
		 * @brief Activate all segments up to the input index.
		 * 
		 * @param input The index to activate up to.
		 * @return Bargraph& For chained operations. 
		 */
		constexpr Bargraph & Assign (unsigned input) noexcept
		{
			Set<count>(input);
			return *this;
		}
		/**
		 * @brief Activate all segments as a ratio of 0:1.
		 * 
		 * @param input The fraction of the bargraph to activate.
		 * @return Bargraph& For chained operations. 
		 */
		constexpr Bargraph & Assign (float input) noexcept
		{
			Assign((unsigned)(input * (float)count + 0.5f));
			return *this;
		}
		/**
		 * @brief Deactivate all bargraph elements
		 * 
		 * @return Bargraph& For chained operations. 
		 */
		constexpr Bargraph & Clear() noexcept
		{
			Assign(*this, 0u);
			return *this;
		}
		/**
		 * @brief Assign the value of a float to the bargraph, this internally calls Assign.
		 * 
		 * @param value The fraction of the bargraph to activate (between 0 and 1)
		 * @return true The input was valid.
		 * @return false The input was invalid.
		 */
		bool operator = (float value) noexcept
		{
			if (0.f <= value && value <= 1.f)
			{
				Assign(value);
				return true;
			}
			return false;
		}
		/**
		 * @brief Assign the value of a float to the bargraph, this internally calls Assign.
		 * 
		 * @param value The fraction of the bargraph to activate (between 0 and 1)
		 * @return true The input was valid.
		 * @return false The input was invalid.
		 */
		bool operator = (double value) noexcept
		{
			return (*this = (float)value);
		}
		/**
		 * @brief Assign up until the index requested.
		 * 
		 * @param value The index to activate until.
		 * @return true The input was valid.
		 * @return false The input was not valid.
		 */
		bool operator = (unsigned value) noexcept
		{
			if (value <= count)
			{
				Assign(value);
				return true;
			}
			return false;
		}
	};
}