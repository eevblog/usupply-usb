#pragma once
#include "AtIndex.hpp"

namespace LCD::Block
{
	/**
	 * @brief Stores the control type of a power supply:
	 * 
	 * CV - Constant Voltage
	 * CC - Constant Current
	 * CP - Constant Power
	 * Off - Power supply is off.
	 */
	enum class Control
	{
		CV,
		CC,
		CP,
		Off
	};
	/**
	 * @brief A class that manages the control mode segments for a LCD screen.
	 * 
	 * @tparam base_class The LCD class.
	 * @tparam ConstantVoltage The constant voltage segment.
	 * @tparam ConstantCurrent The constant current segment.
	 * @tparam ON The ON segment.
	 */
	template <typename base_class, typename ConstantVoltage, typename ConstantCurrent, typename ON>
	struct ControlMode : public base_class
	{
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr ControlMode() = delete;
		constexpr ControlMode(const ControlMode&) = delete;
		constexpr ControlMode& operator=(const ControlMode&) = delete;
		/**
		 * @brief Assign a control mode to the LCD segments.
		 * 
		 * @param input The control mode to assign.
		 * @return ControlMode& For chained operations.
		 */
		constexpr ControlMode & Assign (Control const input) noexcept
		{
			switch (input)
			{
			case Control::CV:
				base_class::template Set<ConstantVoltage>(true);
				base_class::template Set<ConstantCurrent>(false);
				base_class::template Set<ON>(true);
				break;
			case Control::CC:
				base_class::template Set<ConstantVoltage>(false);
				base_class::template Set<ConstantCurrent>(true);
				base_class::template Set<ON>(true);
				break;
			case Control::CP:
				base_class::template Set<ConstantVoltage>(false);
				base_class::template Set<ConstantCurrent>(false);
				base_class::template Set<ON>(true);
				break;
			default:
				base_class::template Set<ConstantVoltage>(false);
				base_class::template Set<ConstantCurrent>(false);
				base_class::template Set<ON>(false);
				break;
			}
			return *this;
		}
		/**
		 * @brief Clears all the control mode segments.
		 * 
		 * @return constexpr ControlMode& 
		 */
		constexpr ControlMode & Clear() noexcept
		{
			Assign(Control::Off);
			return *this;
		}
		/**
		 * @brief Allow direct assignment of the control mode to the ControlMode class.
		 * 
		 * @param input The value to assign.
		 * @return constexpr ControlMode& 
		 */
		constexpr ControlMode & operator = (Control const input) noexcept
		{
			return Assign(input);
		}
	};
}