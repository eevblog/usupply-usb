#pragma once

#include "AtIndex.hpp"

namespace LCD::Block
{
	enum class MultipurposeUnit
	{
		J, W, h, Wh, Off
	};
	/**
	 * @brief This manages the units section given some LCD segments.
	 * 
	 * @tparam base_class The LCD base class.
	 * @tparam J The type of the joules unit segment to manage.
	 * @tparam W The type of the Watts unit segment to manage.
	 * @tparam h The type of the hours segment to manage.
	 */
	template <typename base_class, typename J, typename W, typename h>
	struct MultipurposeUnits : public base_class
	{
		/**
		 * @brief Never a new LCD manager objects.
		 * 
		 * This can only be constructed as via a cast. 
		 * This has a similar pattern to CRTP.
		 */
		constexpr MultipurposeUnits() = delete;
		constexpr MultipurposeUnits(const MultipurposeUnits&) = delete;
		constexpr MultipurposeUnits& operator=(const MultipurposeUnits&) = delete;
		/**
		 * @brief Assign Assign the units to the LCD segments this class manages.
		 * 
		 * @param input The unit to assign.
		 * @return MultipurposeUnits& For chained operations.  
		 */
		constexpr MultipurposeUnits & Assign(MultipurposeUnit input) noexcept
		{
			switch (input)
			{
			case MultipurposeUnit::J:
				base_class::template Set<J>(true);
				base_class::template Set<W>(false);
				base_class::template Set<h>(false);
				break;
			case MultipurposeUnit::W:
				base_class::template Set<J>(false);
				base_class::template Set<W>(true);
				base_class::template Set<h>(false);
				break;
			case MultipurposeUnit::Wh:
				base_class::template Set<J>(false);
				base_class::template Set<W>(true);
				base_class::template Set<h>(true);
				break;
			case MultipurposeUnit::h:
				base_class::template Set<J>(false);
				base_class::template Set<W>(false);
				base_class::template Set<h>(true);
				break;
			default:
				base_class::template Set<J>(false);
				base_class::template Set<W>(false);
				base_class::template Set<h>(false);
				break;
			}
			return *this;
		}
		/**
		 * @brief Clear the multipurpose units.
		 * 
		 * @return MultipurposeUnits& For chained operations.
		 */
		constexpr MultipurposeUnits & Clear() noexcept
		{
			Assign(MultipurposeUnit::Off);
			return *this;
		}
		/**
		 * @brief Assign a unit to the managed units.
		 * 
		 * @param input The unit to assign and set on the LCD.
		 * @return MultipurposeUnits& For chained operations.
		 */
		constexpr MultipurposeUnits & operator = (MultipurposeUnit input) noexcept
		{
			return Assign(input);
		}
	};
}