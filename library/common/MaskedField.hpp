#pragma once
#include <cstdint>

namespace General
{
	/**
	 * @brief A class that is an abstraction to a particular bitfield.
	 * 
	 * This class is used with various vendor bitmasks. Typically they define tables of registers and bitmasks for each bitfield.
	 * 
	 * @tparam T The type of the underlying storage.
	 * @tparam Mask The mask which masks out the particular bitfield.
	 */
	template <typename T, T Mask>
	class MaskedField
	{
	private:
		T & m_Register;

		struct Range
		{
			T Start = 0, Length = 0;
		};
		static constexpr Range GetMaskRange() noexcept
		{
			T mask  = Mask;
			T start = 0u, length = 0u;

			while ( ( mask & 1 ) == 0 )
			{
				++start;
				mask >>= 1;
			}

			while ( ( mask & 1 ) == 1 )
			{
				++length;
				mask >>= 1;
			}

			return {start, length};
		}

		static auto constexpr MaskRange  = GetMaskRange();
		static T constexpr MaskStart  = MaskRange.Start;
		static T constexpr MaskLength = MaskRange.Length;

	public:
		/**
		 * @brief Construct a new Masked Field object
		 * 
		 * @param reg The underlying data.
		 */
		explicit MaskedField( T& reg ) noexcept:
		    m_Register( reg )
		{}
		/**
		 * @brief Clears the bitfield.
		 */
		void Clear() noexcept
		{
			m_Register &= ~Mask;
		}
		/**
		 * @brief Sets the bitfield with a particular value.
		 * 
		 * @param input The value to assign to the bitfield.
		 */
		void Set( T input ) noexcept
		{
			input <<= MaskStart;
			input &= Mask;
			T copy{m_Register & ~Mask};
			m_Register = copy | input;
		}
		/**
		 * @brief Returns the value stored in the bitfield
		 * 
		 * @return auto The value in the bitfield.
		 */
		auto Get() const noexcept
		{
			return ( m_Register & Mask ) >> MaskStart;
		}
		/**
		 * @brief An implicit convertion to the underlying integer type.
		 * 
		 * @return T The value in the bitfield.
		 */
		operator T() const noexcept
		{
			return Get();
		}
		/**
		 * @brief Assigns a value to the bitfield.
		 * 
		 * @param input The value to assign.
		 * @return MaskedField & For chained operations.
		 */
		MaskedField & operator=( T input ) noexcept
		{
			Set( input );
			return *this;
		}
		/**
		 * @brief Ors a value into the bitfield.
		 * 
		 * @param input The value to assign.
		 * @return MaskedField & For chained operations.
		 */
		MaskedField & operator|=( T input ) noexcept
		{
			input <<= MaskStart;
			input &= Mask;
			m_Register |= input;
			return *this;
		}
		/**
		 * @brief Ands a value into the bitfield
		 * 
		 * @param input The value to and with the value in the bitfield
		 * @return MaskedField & For chained operations.
		 */
		MaskedField & operator&=( T input ) noexcept
		{
			input <<= MaskStart;
			input &= Mask;
			m_Register &= ~input;
			return *this;
		}
	};
	/**
	 * @brief Creates a MaskedField from Mask and a reference to the underlying storage.
	 * 
	 * @todo It appears that Mask field is not the correct type, it should be type T. 
	 * 
	 * @tparam Mask The bitfield mask
	 * @tparam T The underlying stored type.
	 * @param reg A reference to the bitfield
	 * @return auto The MaskedField
	 */
	template <std::uint32_t Mask, typename T>
	auto MakeField( T & reg ) noexcept
	{
		return MaskedField<T, (T)Mask>( reg );
	}
}