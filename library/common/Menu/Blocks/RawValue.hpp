#pragma once

#include "Boolean.hpp"
#include "MenuItem.hpp"

namespace Menu
{
	/**
	 * @brief A raw value menu item that displays a value stored in a reference.
	 * 
	 * @tparam BufferSize The number of characters in the render target.
	 * @tparam T The type of the input.
	 */
	template <unsigned BufferSize, typename T>
	class RawValue : public RawMenuItem<BufferSize>
	{
	private:
		T & m_Value;
	public:
		using buffer_t = typename RawMenuItem<BufferSize>::buffer_t;
		/**
		 * @brief Move construct a new Raw Value object.
		 * 
		 * @param input The RawValue to move into this one.
		 */
		RawValue( RawValue && input ) : m_Value( std::move(input.m_Value) ) {}
		/**
		 * @brief Construct a new Raw Value object.
		 * 
		 * @param value A reference to the value type.
		 */
		RawValue( T & value ) : m_Value( value ) {}
		/**
		 * @brief Construct a new Raw Value object.
		 * 
		 * @tparam SizeT<BufferSize> Used by the class deduction guide.
		 * @param value A reference to the value type.
		 */
		RawValue( SizeT<BufferSize>, T & value ) noexcept :
			RawValue( value )
		{}
		/**
		 * @brief Set the referenced value using an input string.
		 * 
		 * @warning This function fails silently.
		 * @param input The string to parse.
		 */
		virtual void Load( buffer_t const& input ) noexcept
		{
			if constexpr ( std::is_floating_point_v<T> )
				atof<BufferSize>( input, m_Value );
			if constexpr ( std::is_unsigned_v<T> )
				atou<BufferSize>( input, m_Value );
			if constexpr ( std::is_integral_v<T> )
				atoi<BufferSize>( input, m_Value );
			if constexpr ( General::is_boolean_v<T> )
				m_Value.FromString( input );
		}
		/**
		 * @brief Renders the value in the reference to the render target.
		 * 
		 * @param output The render target, a character array.
		 * @param use_cursor Whether or not to use cursors (does nothing here).
		 * @return unsigned The length of the rendered section.
		 */
		virtual unsigned Render( buffer_t & output, bool use_cursor ) noexcept
		{
			if constexpr ( std::is_floating_point_v<T> )
				return fixed_ftoa<BufferSize>( m_Value, output );

			if constexpr ( std::is_unsigned_v<T> )
				return fixed_utoa<BufferSize>( m_Value, output );

			if constexpr ( std::is_integral_v<T> )
				return fixed_itoa<BufferSize>( m_Value, output );

			if constexpr ( std::is_same_v<std::decay_t<T>, bool> )
				return fixed_btoa<BufferSize>( m_Value, output );

			if constexpr ( General::is_boolean_v<T> )
				return m_Value.ToString( output );

			return 0u;
		}
	};
	/**
	 * @brief A template class deduction guide.
	 * 
	 * This allows the RawValue to be constructed using a passed in RenderTarget value.
	 * 
	 * @tparam BufferSize The number of characters in the render target.
	 * @tparam T The type that is referenced internally in the RawValue.
	 */
	template <unsigned BufferSize, typename T>
	RawValue(SizeT<BufferSize>, T&) -> RawValue<BufferSize, std::decay_t<T>>;
}