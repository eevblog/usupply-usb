#pragma once
#include <tuple>
#include <type_traits>
#include <string.h>
//
#define AUTO_MEMBER(name, v) decltype(v) name = v
//
namespace General
{
	/**
	 * @brief Checks whether a type is castable From one type To another.
	 * 
	 * @tparam To The type to cast from.
	 * @tparam From The type to cast to.
	 */
	template <typename To, typename From>
	inline constexpr bool BitCastable_v = (sizeof(To) <= sizeof(From)) && std::is_trivially_copyable<From>::value && std::is_trivial<To>::value;
	/**
	 * @brief Converts a trivial type to another type, converting at a byte level.
	 * 
	 * @tparam To The type to copy into.
	 * @tparam From The type to copy from.
	 * @param src The source.
	 * @return To The cast value. 
	 */
	template <class To, class From>
	auto PunCast(const From &src) noexcept -> typename std::enable_if<BitCastable_v<To, From>, To>::type
	{
		To dst;
		memcpy(&dst, &src, sizeof(To));
		return dst;
	}
	/**
	 * @brief A Metaprogramming tool used for mirroring a type.
	 * 
	 * @tparam T 
	 */
	template <typename T>
	struct Mirror
	{
		using type = T;
	};
	template <typename T>
	using Mirror_t = typename Mirror<T>::type;
	/**
	 * @brief SizeT wrapper
	 * 
	 * less verbose than integral constant
	 * 
	 * @tparam v The value.
	 */
	template <std::size_t v>
	struct SizeT { static constexpr std::size_t value = v; };
	/**
	 * @brief A type wrapper, the same as Mirror<T>, but with a more expressive name in some contexts.
	 * 
	 * @tparam T 
	 */
	template <typename T>
	struct Type { using type = T; };
	template <typename T>
	using Type_t = typename Type<T>::type;
	/**
	 * 
	 */
	#pragma pack(push, 1)
	/**
	 * @brief This is a meta-programming tool.
	 * 
	 * It lets you use one type to expand another.
	 * For example:
	 * 
	 * If you had this:
	 * TypeList<int, double, bool>
	 * 
	 * 	Where Args... = int, double, bool
	 * 
	 * And you wanted to create a manager for each of these:
	 * 	
	 * 	FirstType<Manager, Args>::type ...
	 */
	template <typename T, typename K>
	struct FirstType{ using type = T; };
	//
	template <typename T, typename K>
	using FirstType_t = typename FirstType<T, K>::type;
	#pragma pack(pop)
	/**
	 * @brief Stores an arbitary value in a template, for use in metaprogramming.
	 * 
	 * @tparam v 
	 */
	template <auto v>
	struct Key
	{
		using type = std::decay_t<decltype(v)>;
		static constexpr auto value = v; 
	};
	/**
	 * @brief Gets the underlying type of an enum.
	 * 
	 * @tparam T The enum type.
	 * @tparam Enable Whether or not the function is an enum. 
	 */
	template <class T, class Enable = void>
	struct UnderlyingType
	{
		using type = T;
	};
	template <class T>
	struct UnderlyingType<T, typename std::enable_if<std::is_enum<T>::value>::type>
	{
		using type = std::underlying_type_t<T>;
	};
	/**
	 * @brief Returns the underlying value inside an enum.
	 * 
	 * @tparam T The enum type.
	 * @tparam U The underlying type. 
	 * @param v The enum.
	 * @return U The underlying enum value.
	 */
	template <typename T, typename U = std::underlying_type_t<T> >
	constexpr auto UnderlyingValue(T v) -> U
	{
		return static_cast<U>(v);
	}
	/**
	 * @brief Converts true to 1, false to 0.
	 * 
	 * @param c 
	 * @return constexpr int 
	 */
	constexpr int TrueIsOne(bool c) { return (c) ? 1 : 0; }
	/**
	 * @brief Converts 1 to true and 0 to false.
	 * 
	 * @param c 
	 * @return true 
	 * @return false 
	 */
	constexpr bool OneIsTrue(int c) { return !!(c); }
	/**
	 * @brief Tests whether a type is a CString.
	 * 
	 * @tparam T 
	 */
	template <typename T>
	constexpr bool IsCString_v = false;
	template <unsigned N>
	constexpr bool IsCString_v<const char(&)[N]> = true;
	template <unsigned N>
	constexpr bool IsCString_v<char(&)[N]> = true;
	template <unsigned N>
	constexpr bool IsCString_v<const char[N]> = true;
	template <unsigned N>
	constexpr bool IsCString_v<char[N]> = true;
	/**
	 * @brief Creates a mask at the bit.
	 * 
	 * @tparam T The type of the bit.
	 * @param bit The bit to create a mask for.
	 * @return T The masked for the bit.
	 */
	template <typename T>
	constexpr auto MaskBit( T const & bit ) noexcept
	{
		return (std::decay_t<T>)1 << bit;
	}
	/**
	 * @brief Creates a mask for multiple bits.
	 * 
	 * Example:
	 * 
	 * If you wanted to mask bits 1 and 7 in a bitfield:
	 * 
	 * auto mask_1_and_7 = MultiMaskBit(1, 7);
	 * 
	 * @tparam T The type of the resultant mask
	 * @param bits The bits to mask
	 * @return T A mask of all the defined bits.
	 */
	template <typename... T>
	constexpr auto MultiMaskBit( T const &... bits ) noexcept
	{
		return ( ... | (std::common_type_t<T...>)MaskBit( bits ) );
	}
	/**
	 * @brief Bitwise Ors the data.
	 * 
	 * @tparam T The type of the data.
	 * @param data The data
	 * @return T The bitwise or of the parameters. 
	 */
	template <typename... T>
	constexpr auto Or( T const &... data ) noexcept
	{
		return ( ... | (std::common_type_t<T...>)data );
	}
	/**
	 * @brief Creates a bitmask of "length" starting at bit "start".
	 * 
	 * @tparam T The type for the bitmask
	 * @param start The first bit to mask.
	 * @param length The number of bits to ask.
	 * @return T The masked range.
	 */
	template <typename T>
	constexpr auto MaskRange( T const & start, Type_t<T> const & length ) noexcept -> T
	{
		auto working_mask = MaskBit( start );
		auto output       = working_mask;

		for ( T i = (T)0; i < length; ++i )
		{
			output |= working_mask;
			working_mask <<= 1u;
		}
		return output;
	}
	/**
	 * @brief Gets a bit from a bitmask range.
	 * 
	 * Example:
	 * 
	 * If you wanted to see the values of two bits 3 and 4 in a register.
	 * 
	 * There are two bits in the range which starts at bit 3.
	 * auto bits3_4 = FromRange(register, 3, 2);
	 * 
	 * @tparam T The type of the bitmask.
	 * @param input The input for to extract the data from.
	 * @param start The first bit of the bitfield.
	 * @param length The number of bits to extract.
	 * @return T value stored in the bitfield defined by the range start and length. 
	 */
	template <typename T>
	constexpr T FromRange( T const& input, Type_t<T> const& start, Type_t<T> const& length ) noexcept
	{
		auto const mask = MaskRange<T>( start, length );
		return ( input & mask ) >> start;
	}
	/**
	 * @brief Shifts a value into the correct range in an integer.
	 * 
	 * Example:
	 * 
	 * If you wanted to assign two bits in a register 3 and 4 with the value 3 then:
	 * 
	 * register |= ToRange( register, 3, 2 );
	 * 
	 * @tparam T The type of the register.
	 * @param input The register.
	 * @param start The start of the bitmask
	 * @param length The number of bits masked.
	 * @return T The Resulting value in the register.
	 */
	template <typename T>
	constexpr T ToRange( T const& input, Type_t<T> const& start, Type_t<T> const& length ) noexcept
	{
		auto const mask = MaskRange<T>( 0u, length );
		return ( input & mask ) << start;
	}
	/**
	 * @brief Checks if any of the parameters match the input.
	 * 
	 * This does not cast the test to the type of the input.
	 * It is possible to compare two disimilar types if they have the operator== overload.
	 * 
	 * @tparam T The type of the input.
	 * @tparam Args The argument types to compare with the input.
	 * @param input The input.
	 * @param tests The values to compare with the input.
	 * @return true Atleast one of the values operator== evaulates to true.
	 * @return false None of the operator== evaluate to true.
	 */
	template <typename T, typename... Args>
	constexpr bool IsOneOf( T const& input, Args const&... tests ) noexcept
	{
		return ( ... || ( input == tests ) );
	}
	/**
	 * @brief Checks if any of the parameters match the input.
	 * 
	 * The args type must be convertable to the type of input.
	 * 
	 * @tparam T The type of the input.
	 * @tparam Args The argument types to compare with the input.
	 * @param input The input.
	 * @param tests The values to compare with the input.
	 * @return true Atleast one of the values operator== evaulates to true.
	 * @return false None of the operator== evaluate to true.
	 */
	template <typename T, typename... Args>
	constexpr bool IsAnyOf( T const& input, Args const&... tests ) noexcept
	{
		return ( ... || ( input == static_cast<T>(tests) ) );
	}
	/**
	 * @brief Checks if ALL the tests evaulate to true.
	 * 
	 * @tparam T The type of the input.
	 * @tparam Args The types of the tests.
	 * @param input The input.
	 * @param tests The test values.
	 * @return true All of the input == tests values evaluate to true.
	 * @return false Atleast one of the inputs don't evaluate to true.
	 */
	template <typename T, typename... Args>
	constexpr bool IsAllOf( T const& input, Args const&... tests ) noexcept
	{
		return ( ... && ( input == static_cast<T>(tests) ) );
	}
	/**
	 * @brief Iterates through arbitray indexed values stored in tuple.
	 * 
	 * @tparam Tuple The tuple type (anything that supports std::get )
	 * @tparam F The function to call for each different tuple element.
	 * @tparam Indices The indexes to extract in the tuple.
	 * @param tuple The tuple.
	 * @param f The callback.
	 */
	template <typename Tuple, typename F, std::size_t... Indices>
	constexpr void ForEachTuple(Tuple &&tuple, F &&f, std::index_sequence<Indices...>) noexcept
	{
		( std::forward<F>(f)( std::get<Indices>( std::forward<Tuple>( tuple ) ), std::integral_constant<std::size_t, Indices>{} ), ... );
	}
	/**
	 * @brief Iterates through ALL values stored in tuple.
	 * 
	 * @tparam Tuple The tuple type (anything that supports std::get )
	 * @tparam F The function to call for each different tuple element.
	 * @param tuple The tuple.
	 * @param f The callback.
	 */
	template <typename Tuple, typename F>
	constexpr void ForEachTuple(Tuple &&tuple, F &&f) noexcept
	{
		constexpr std::size_t N = std::tuple_size<std::remove_reference_t<Tuple>>::value;
		ForEachTuple(std::forward<Tuple>(tuple), std::forward<F>(f), std::make_index_sequence<N>{});
	}
	/**
	 * @brief Iterate through arbitary elements in a tuple until one evaluates to false.
	 * 
	 * @tparam Tuple The tuple type (anything that supports std::get )
	 * @tparam F The function to call for each different tuple element.
	 * @tparam Indices The indexes to extract in the tuple.
	 * @param tuple The tuple.
	 * @param f The callback.
	 * @return true when the function evaluates to true for all tuple elements.
	 * @return false The callback function evaluated to false for atleast 1 tuple element.
	 */
	template <typename Tuple, typename F, std::size_t... Indices>
	constexpr bool ForEachAndTuple(Tuple &&tuple, F &&f, std::index_sequence<Indices...>) noexcept
	{
		return ( std::forward<F>(f)( std::get<Indices>( std::forward<Tuple>( tuple ) ), std::integral_constant<std::size_t, Indices>{} ) && ... );
	}
	/**
	 * @brief Iterate through ALL elements in a tuple until one evaluates to false.
	 * 
	 * @tparam Tuple The tuple type (anything that supports std::get )
	 * @tparam F The function to call for each different tuple element.
	 * @param tuple The tuple.
	 * @param f The callback.
	 * @return true when the function evaluates to true for all tuple elements.
	 * @return false The callback function evaluated to false for atleast 1 tuple element.
	 */
	template <typename Tuple, typename F>
	constexpr bool ForEachAndTuple(Tuple &&tuple, F &&f) noexcept
	{
		constexpr std::size_t N = std::tuple_size<std::remove_reference_t<Tuple>>::value;
		return ForEachAndTuple(std::forward<Tuple>(tuple), std::forward<F>(f), std::make_index_sequence<N>{});
	}
	/**
	 * @brief Decays the rvalue reference into a value but leaves lvalue references as lvalue references.
	 * 
	 * @tparam T The type to decay.
	 */
	template <typename T>
	struct decay_rvalue
	{
		using type = T;
	};
	template <typename T>
	struct decay_rvalue<T &&>
	{
		using type = T;
	};
	template <typename T>
	struct decay_rvalue<T const && >
	{
		using type = T const;
	};
	template <typename T>
	struct decay_rvalue<T &>
	{
		using type = T &;
	};
	template <typename T>
	struct decay_rvalue<T const &>
	{
		using type = T const &;
	};
	/**
	 * @brief A type alias for decay_rvalue.
	 * 
	 * @tparam T The type to decay,
	 */
	template <typename T>
	using decay_rvalue_t = typename decay_rvalue<T>::type;
	/**
	 * @brief Gets the return value of simple lambda functions, doesn't support all formats of lambdas.
	 * 
	 * @tparam T The lamda type.
	 */
	template <typename T>
	struct LambdaReturn : LambdaReturn< decltype( &T::operator( ) ) >
	{};
	template < typename ClassType, typename ReturnType, typename... Args >
	struct LambdaReturn< ReturnType( ClassType::*)( Args... ) const>
	{
		using type = ReturnType;
	};
	template < typename ClassType, typename ReturnType, typename... Args >
	struct LambdaReturn< ReturnType( ClassType::* )( Args... )>
	{
		using type = ReturnType;
	};
	template < typename T >
	using LambdaReturn_t = typename LambdaReturn<T>::type;
}