#pragma once
#include "InterruptRegisters.hpp"
#include <cstdint>

namespace IO
{
	/**
	 * @brief A GPIO port.
	 */
	enum class Port : std::uint32_t
	{
		A = 0, //17u,
		B = 1, //18u,
		C = 2, //19u,
		D = 3, //20u,
		E = 4, //21u,
		F = 5, //22u
	};
	/**
	 * @brief A Pin state
	 * 
	 */
	enum class State : bool
	{
		Low  = false,
		High = true,
	};
	/**
	 * @brief The type of drive for a pin.
	 * 
	 */
	enum class Type : std::uint16_t
	{
		PushPull  = 0b0,
		OpenDrain = 0b1
	};
	/**
	 * @brief The pins mode.
	 * 
	 */
	enum class Mode : std::uint32_t
	{
		Input     = 0b00,
		Output    = 0b01,
		Alternate = 0b10,
		Analog    = 0b11
	};
	/**
	 * @brief The speed for the pin.
	 * 
	 */
	enum class Speed : std::uint32_t
	{
		Low      = 0b00,
		Medium   = 0b01,
		High     = 0b10,
		VeryHigh = 0b11
	};
	/**
	 * @brief The pullups used for a pin.
	 * 
	 */
	enum class Resistors : std::uint32_t
	{
		None     = 0b00,
		PullUp   = 0b01,
		PullDown = 0b10
	};
	/**
	 * @brief Which interrupt edge is used.
	 * 
	 */
	using InterruptEdge = System::InterruptGeneral::InterruptEdge;
	/**
	 * @brief The interrupt routing class.
	 * 
	 */
	using Interrupt = System::InterruptGeneral::Interrupt;
	/**
	 * @brief The interrupt event.
	 * 
	 */
	using Event = System::InterruptGeneral::Event;
	/**
	 * @brief The default alternate function for a pin (nothing.)
	 * 
	 * @tparam Port The port type. 
	 * @tparam std::uint32_t The pin number.
	 */
	template <Port, std::uint32_t>
	struct AlternateFunction
	{
		enum Type
		{
			Default = 0
		} m_Value;
	};
	/**
	 * @brief A standin for a non-existant pin
	 * 
	 * Used for debugging and development.
	 */
	struct NoPin
	{
		NoPin( ... ) {}
		operator bool() const { return false; }
		void operator=( bool const ) {}
	};
}