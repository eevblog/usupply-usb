#pragma once

namespace General
{
	/**
	 * @brief This is used as a generic reference to a bit in an arbitary data structure.
	 * 
	 * The Singleton_t type has some requirements for this structure:
	 * - It must support |= operator
	 * - It must support &= operator
	 * - It must support ^= operator
	 * - It must be implictly convertable to the type of Bit unsigned integer type. (unsigned, uint32_t etc.)
	 * 
	 * @tparam Singleton_t 
	 * @tparam Bit The bit to extract from the arbitary data structure. Must be an unsigned integer type.
	 */
	template <typename Singleton_t, auto Bit>
	class ReferenceBit
	{
	private:
		Singleton_t m_Value;
	public:
		/**
		 * @brief Extracts the bit and returns a bool that represents it.
		 * 
		 * @return true If the bit is 1.
		 * @return false If the bit is 0.
		 */
		operator bool() const noexcept
		{
			return (bool)( ( m_Value >> Bit ) & 1 );
		}
		/**
		 * @brief Assigns the bit the value of a boolean.
		 * 
		 * @param value The value to check against.
		 * @return ReferenceBit& For chained operations.
		 */
		ReferenceBit & operator=( bool value ) noexcept
		{
			if ( value ) m_Value |= ( 1 << Bit );
			else		 m_Value &= ~( 1 << Bit );
			return *this;
		}
		/**
		 * @brief Ors the bit with the value of a boolean.
		 * 
		 * @param value The value to or the bit with.
		 * @return ReferenceBit& For chained operations.
		 */
		ReferenceBit & operator|=( bool value ) noexcept
		{
			m_Value |= ( (unsigned)value << Bit );
			return *this;
		}
		/**
		 * @brief Ands the bit with the value of a boolean.
		 * 
		 * @param value The value to and the bit with.
		 * @return ReferenceBit& For chained operations.
		 */
		ReferenceBit & operator&=( bool value ) noexcept
		{
			m_Value &= ( (unsigned)value << Bit );
			return *this;
		}
		/**
		 * @brief XORs the value of the bit with a boolean.
		 * 
		 * @param value The value to xor the bit with.
		 * @return ReferenceBit& For chained operations.
		 */
		ReferenceBit & operator^=( bool value ) noexcept
		{
			m_Value ^= ( (unsigned)value << Bit );
			return *this;
		}
	};
}