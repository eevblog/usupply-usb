#pragma once
#include <utility>

namespace General
{
    /**
     * @brief Run a function that is passed into the constructor of this class.
     * 
     * This is used to run a function before certain class members are intialised, such as inherited members.
     */
    struct RunOnConstruct
    {
        /**
         * @brief Construct nothing.
         * 
         */
        constexpr RunOnConstruct() = default;
        /**
         * @brief Runs the function passed into the constructor.
         * 
         * @tparam F The type of the function.
         * @param function The function to run.
         */
        template <typename F>
        constexpr RunOnConstruct(F && function)
        {
            std::forward<F>(function)();
        }
    };
}