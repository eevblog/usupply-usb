#pragma once

#include "Char.hpp"
#include "ParseResult.hpp"

namespace Parser::Folding
{
	/**
	 * @brief Compares two strings and returns the matched length if the strings match.
	 * 
	 * @note Reduces binary size (GCC 7.2, ld)
	 * properly optimised with comdat folding enabled, still better
	 * 
	 * @param input The input string.
	 * @param M The length of the input string.
	 * @param data The match data.
	 * @param L The length of the match data.
	 * @param index The index to start comparing (the offset from the input strings start).
	 * @return ParseResult The result of the comparison.
	 */
	constexpr ParseResult Match(const char * input, std::size_t const M, const char * data, std::size_t const L, std::size_t index) noexcept
	{
		std::size_t i = 0u;
		while (((i + index) < M) && (i < L))
		{
			if (!General::Equivalent(data[i], input[i + index])) return false;
			++i;
		}
		return (i == L) ? ParseResult(i) : ParseResult(false);
	}
	/**
	 * @brief Checks whether a string exists and return true if it does.
	 * 
	 * This is used to ignore a part of a string.
	 * 
	 * @note Increases binary size (GCC 7.2, ld) 
	 * 	when used for raw IgnoreOnce in BasicBlock properly optimised with comdat folding enabled, still better
	 * @param input The input array.
	 * @param N The input length
	 * @param data The data to check.
	 * @param L The length of the data to check.
	 * @param index The index to start checking.
	 * @return true The input exists in the data starting at index.
	 * @return false No input exists in the data starting at index.
	 */
	constexpr bool IgnoreOnce(char const * input, std::size_t const N, const char * data, std::size_t const L, std::size_t index) noexcept
	{
		if ( index < N )
		{
			auto const output = input[ index ];
			for ( auto i = 0u; i < L; ++i ) 
				if ( data[i] == output )
					return true;
		}
		return false;
	}
}