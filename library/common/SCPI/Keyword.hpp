#pragma once

#include "Hide.hpp"
#include "Required.hpp"
#include "Optional.hpp"
#include "Convert.hpp"

namespace Parser
{
	/**
	 * @brief A SCPI keyword class, used to store the required and optional components.
	 * 
	 * @tparam RequiredLength The length of the required section.
	 * @tparam OptionalLength The length of the optional section.
	 */
	template <std::size_t RequiredLength, std::size_t OptionalLength>
	class Keyword;
	/**
	 * @brief A SCPI keyword class, used to store the required and optional components.
	 * 
	 * @tparam RequiredLength The length of the required section.
	 * @tparam OptionalLength The length of the optional section.
	 */
	template <std::size_t RequiredLength, std::size_t OptionalLength>
	class Keyword : General::Hide<Required<RequiredLength>>, General::Hide<Optional<OptionalLength>>
	{
	private:
		using required_t		= Required<RequiredLength>;
		using optional_t		= Optional<OptionalLength>;
		using hide_required_t	= General::Hide<required_t>;
		using hide_optional_t	= General::Hide<optional_t>;
		/**
		 * @brief Get the Base object
		 * 
		 * @tparam T The type to retrieve (optional or required type)
		 * @return decltype(auto) The requested type.
		 */
		template <typename T>
		constexpr decltype(auto) GetBase() const noexcept
		{
			return T::Get();
		}
	public:
		/**
		 * @brief Construct a SCPI keyword from two C strings.
		 * 
		 * @tparam R The length of the required section.
		 * @tparam O The length of the optional section.
		 */
		template <std::size_t R, std::size_t O>
		constexpr Keyword(char const (&required)[R], char const (&optional)[O]) noexcept :
			hide_required_t{ General::MakeArray(required) },
			hide_optional_t{ General::MakeArray(optional) }
		{}
		/**
		 * @brief Allow copy construction.
		 */
		constexpr Keyword(Keyword const & input) noexcept = default;
		/**
		 * @brief Parse the keyword and return an affirmative ParseResult if it is detected.
		 * 
		 * @tparam N The length of the input array.
		 * @param input The input array.
		 * @param index The index to start parsing.
		 * @param offset The offset from the index to parse.
		 * @return ParseResult The parse result, see ParseResult docs.
		 */
		template <std::size_t N>
		ParseResult constexpr operator () (std::array<char, N> const & input, std::size_t index = 0, std::size_t offset = 0) const noexcept
		{
			if (auto r1 = (GetBase<hide_required_t>())(input, index, offset); r1)
				if (auto r2 = (GetBase<hide_optional_t>())(input, index, offset + r1.Length()); r2)
					return { r1.Length() + r2.Length() };

			return { false };
		}

		using result_t = ParseResult;
	};
	/**
	 * @brief Deduce the size of the required and optional sections of the keyword.
	 * 
	 * @tparam R The length of the required section.
	 * @tparam O The length of the optional section.
	 */
	template <std::size_t R, std::size_t O>
	Keyword(const char(&)[R], const char(&)[O])->Keyword<R - 1, O - 1>;
}