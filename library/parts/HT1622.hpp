#pragma once

#include "Integer.hpp"
#include "SPIBitbanging.hpp"

namespace Parts::HT1622
{
	/**
	 * @brief The number of databits used in transactions to the HT1622.
	 */
	static constexpr unsigned data_bits    = 4;
	/**
	 * @brief The number of address bits used in transactions to the HT1622.
	 */
	static constexpr unsigned address_bits = 6;
	/**
	 * @brief The number of command bits used in trasactions to the HT1622.
	 */
	static constexpr unsigned command_bits = 3;
	/**
	 * @brief Unsigned type based on the required data bits.
	 */
	using DataInt = General::UnsignedInt_t<data_bits>;
	/**
	 * @brief The different states supported by the HT1622 LCD driver.
	 */
	enum class Commands : std::uint16_t
	{
		SYS_DIS   = 0b000000000,
		SYS_EN    = 0b000000010,
		LCD_OFF   = 0b000000100,
		LCD_ON    = 0b000000110,
		TIMER_DIS = 0b000001000,
		WDT_DIS   = 0b000001010,
		TIMER_EN  = 0b000001100,
		WDT_EN    = 0b000001110,
		TONE_OFF  = 0b000010000,
		CLR_TIMER = 0b000011010,
		CLR_WDT   = 0b000011110,
		RC_32K    = 0b000110000,
		E0T_32K   = 0b000111000,
		TONE_4K   = 0b010000000,
		TONE_2K   = 0b011000000,
		IRQ_DIS   = 0b100000000,
		IRQ_EN    = 0b100010000,
		F1        = 0b101000000,
		F2        = 0b101000010,
		F4        = 0b101000100,
		F8        = 0b101000110,
		F16       = 0b101001000,
		F32       = 0b101001010,
		F64       = 0b101001100,
		F128      = 0b101001110,
		TEST      = 0b111000000,
		NORMAL    = 0b111000110
	};
	/**
	 * @brief Get the address object
	 * 
	 * @param common 
	 * @param segment 
	 * @return auto 
	 */
	static constexpr unsigned GetAddress( unsigned common, unsigned segment )
	{
		bool const nibble = common / 4u;
		return ( ( segment << 1u ) | (unsigned)nibble );
	}
	/**
	 * @brief Get the bit of the common line.
	 * 
	 * @param common The common line to select.
	 * @return DataInt The integer that represents the common line.
	 */
	static constexpr DataInt GetBit( unsigned common )
	{
		bool const nibble = common / 4u;
		if ( nibble ) ( common -= 4u );
		return ( 1u << common );
	}
	/**
	 * @brief A manager class for the HT1622.
	 * 
	 * @tparam SPI_Kernal The underlying SPI implementation.
	 */
	template <typename SPI_Kernal>
	class HT1622
	{
	private:
		SPI_Kernal m_SPI;
		/**
		 * @brief The data for a start transaction for the HT1622.
		 */
		union WriteStart
		{
			static constexpr unsigned packet_bits  = address_bits + command_bits;
			using integer                          = General::UnsignedInt<packet_bits>;
			using integer_t                        = General::UnsignedInt_t<packet_bits>;
			static constexpr unsigned padding_bits = integer::padding;

			integer_t Bits;

			struct
			{
				unsigned Address : address_bits;
				unsigned Command : command_bits;
				unsigned Padding : padding_bits;
			};
		};
		/**
		 * @brief The data in a single packet sent to the HT1622.
		 */
		union WritePacket
		{
			static constexpr unsigned packet_bits = data_bits + address_bits + command_bits;
			using integer                         = General::UnsignedInt<packet_bits>;
			using integer_t                       = General::UnsignedInt_t<packet_bits>;

			integer_t                 Bits;
			static constexpr unsigned padding_bits = integer::padding;

			struct
			{
				unsigned Data : data_bits;
				unsigned Address : address_bits;
				unsigned Command : command_bits;
				unsigned Padding : padding_bits;
			};
		};
		/**
		 * @brief The data for a command sent to the HT1622.
		 */
		union CommandPacket
		{
			static constexpr unsigned command_code_bits = 9;
			static constexpr unsigned packet_bits       = command_code_bits + command_bits;
			using integer                               = General::UnsignedInt<packet_bits>;
			using integer_t                             = General::UnsignedInt_t<packet_bits>;

			integer_t                 Bits;
			static constexpr unsigned padding_bits = integer::padding;

			struct
			{
				unsigned Code : command_code_bits;
				unsigned Command : command_bits;
				unsigned Padding : padding_bits;
			};
		};
	public:
		/**
		 * @brief Construct a new HT1622 object
		 * 
		 * Setup the HT1622 chip and get it ready for writes.
		 */
		HT1622() noexcept
		{
			Command( Commands::NORMAL );
			Command( Commands::SYS_EN );
			Command( Commands::RC_32K );
			Command( Commands::TIMER_DIS );
			Command( Commands::WDT_DIS );
			Command( Commands::TONE_OFF );
			Command( Commands::LCD_ON );
		}
		/**
		 * @brief Write an integer to the requested address.
		 * 
		 * @param address The address to write.
		 * @param data The data to write.
		 * @return true The write succeeded.
		 * @return false The write failed.
		 */
		bool Write( General::UnsignedInt_t<address_bits> address, DataInt data ) noexcept
		{
			if ( address >= General::UnsignedInt<address_bits>::max ) return false;
			if ( data >= General::UnsignedInt<data_bits>::max ) return false;

			WritePacket temp{0u};

			temp.Data    = data;
			temp.Address = address;
			temp.Command = 0b101;

			m_SPI.template Write<WritePacket::packet_bits>( temp.Bits );
			return true;
		}
		/**
		 * @brief Writes a contigious block of data to the HT1622.
		 * 
		 * @tparam count The number of bytes to write.
		 * @param address The address to write.
		 * @param data The data to write.
		 * @return true The write succeeded.
		 * @return false The write failed.
		 */
		template <unsigned count>
		bool WriteBulk( General::UnsignedInt_t<address_bits> address, std::array<DataInt, count> data ) noexcept
		{
			if ( address >= General::UnsignedInt<address_bits>::max ) return false;
			WriteStart temp{0u};
			temp.Address = address;
			temp.Command = 0b101;

			// Reverses the bits for transmission
			static const std::uint8_t reverse_lookup[ 16 ] = {0b0000, 0b1000, 0b0100, 0b1100, 0b0010, 0b1010, 0b0110, 0b1110, 0b0001, 0b1001, 0b0101, 0b1101, 0b0011, 0b1011, 0b0111, 0b1111};

			m_SPI.WriteStart();
			m_SPI.template WriteData<WriteStart::packet_bits>( temp.Bits );
			for ( auto&& data : data ) m_SPI.template WriteData<data_bits>( reverse_lookup[ ( data & 0xF ) ] );
			m_SPI.WriteEnd();
			return true;
		}
		/**
		 * @brief Issues a command to the HT1622.
		 * 
		 * @param command The command to issue.
		 */
		void Command( Commands command ) noexcept
		{
			CommandPacket temp{0u};
			temp.Code    = (std::uint16_t)command;
			temp.Command = 0b100;
			m_SPI.template Write<CommandPacket::packet_bits>( temp.Bits );
		}
	};
}