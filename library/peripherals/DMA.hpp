#pragma once

#include "Power.hpp"
#include "Interrupt.hpp"
#include "RegistersDMA.hpp"
#include "StaticLambdaWrapper.hpp"
#include <cstdint>

namespace Peripherals
{
	using System::InterruptSource;
	/**
	 * @brief This represents which DMA interrupts are enabled.
	 */
	enum class DMAInterruptFlag
	{
		HalfComplete,
		Complete,
		Error
	};
	/**
	 * @brief Get the interrupt source for a given DMA channel.
	 * 
	 * @tparam Channel The DMA channel to get the interrupt source of.
	 * @return InterruptSource The interrupt source for the DMA channel.
	 */
	template <size_t Channel>
	InterruptSource constexpr DMAChannelInterrupt() noexcept
	{
		switch (Channel)
		{
		case 1: return InterruptSource::eDMA_CH1;
		case 2: case 3: return InterruptSource::eDMA_CH2_3;
		case 4: case 5: case 6: case 7: return  InterruptSource::eDMA_CH4_5_6_7;
		};
	}
	/**
	 * @brief Manages the power on and off of a DMA channel.
	 */
	template <size_t Channel, size_t Priority>
	struct DMAPowerKernal
	{
		/**
		 * @brief The interrupt channel for the DMA channel.
		 */
		static constexpr auto ChannelInterrupt = (IRQn_Type)DMAChannelInterrupt<Channel>();
		/**
		 * @brief Gets the DMA channel ready for operation and enable the NVIC interrupt.
		 */
		static void Construct() noexcept
		{
			RCC->AHBENR |= RCC_AHBENR_DMAEN;
			NVIC_EnableIRQ( ChannelInterrupt );
			NVIC_SetPriority( ChannelInterrupt, Priority );
		}
		/**
		 * @brief Disables the DMA module and its interrupt.
		 */
		static void Destruct() noexcept
		{
			RCC->AHBENR &= ~RCC_AHBENR_DMAEN;
			NVIC_DisableIRQ( ChannelInterrupt );
		}
	};
	/**
	 * @brief The DMA properties of the dma peripheral and channel.
	 * 
	 * @tparam Module The particular DMA module.
	 * @tparam Channel The DMA channel on the module.
	 * @tparam Priority The interrupt priority of the DMA interrupts.
	 */
	template<size_t Module, size_t Channel, size_t Priority = 2>
	struct DMAProperties
	{
		using REGS			= DMAGeneral::Registers<Module, Channel>;
		using CCR  			= typename REGS::CCR_t;
		using CPAR 			= typename REGS::CPAR_t;
		using CMAR 			= typename REGS::CMAR_t;
		using CNDTR			= typename REGS::CNDTR_t;
		using ISR 			= typename REGS::ISR_t;
		using IFCR 			= typename REGS::IFCR_t;
		/**
		 * @brief Gets the remaining bytes to transmit in the DMA
		 * 
		 * @return size_t The number of bytes remaining.
		 */
		static size_t Remaining() noexcept
		{
			return ( ISR().TCIF().Get() ) ? 0 : CNDTR().Get();
		}
		/**
		 * @brief Get the MemorySize per each DMA transaction to/from memory.
		 * 
		 * @tparam Size The number of bytes to use for memory transactions.
		 * @return DMAGeneral::MemorySize The MemorySize of the memory transaction.
		 */
		template <size_t Size>
		static constexpr DMAGeneral::MemorySize GetMemorySize() noexcept
		{
			switch(Size)
			{
			case 1: return DMAGeneral::MemorySize::Bits8;
			case 2: return DMAGeneral::MemorySize::Bits16;
			case 4: return DMAGeneral::MemorySize::Bits32;
			}
		}
		/**
		 * @brief Gets whether or not the DMA is enabled.
		 * 
		 * @return true Channel is enabled.
		 * @return false Channel is disabled.
		 */
		static bool Enabled() noexcept
		{
			return CCR().EN();
		}
		/**
		 * @brief Disables a DMA channel.
		 * 
		 * @warning This WILL block until the channel disables.
		 */
		static void Disable() noexcept
		{
			IFCR().CGIF() = true;
			CCR().EN() = false;
			while( Enabled() );
		}
		/**
		 * @brief Enables a DMA channel.
		 * 
		 * @warning This WILL block until the channel enables.
		 */
		static void Enable() noexcept
		{
			CCR().EN() = true;
			while( not Enabled() );
		}
		/**
		 * @brief Sets up a memory transation for the DMA channel.
		 * 
		 * @tparam Memory The type of the memory.
		 * @param memory A pointer to the memory at the start of the DMA transaction.
		 * @param length The number of items that are in the transaction.
		 */
		template <typename Memory>
		static void SetupMemory( Memory * memory, std::size_t length ) noexcept
		{
			Disable();
			//
			// 1. Write the USART_TDR register address in the DMA control register to configure it as
			// the destination of the transfer. The data is moved to this address from memory after
			// each TXE event.
			
			//
			// 2. Write the memory address in the DMA control register to configure it as the source of
			// the transfer. The data is loaded into the USART_TDR register from this memory area
			// after each TXE event.
			CMAR().MA() = (std::uint32_t)memory;
			//
			// 3. Configure the total number of bytes to be transferred to the DMA control register.
			CNDTR().NDT() = length;
			//
			// 4. Configure the channel priority in the DMA register
			
			//
			// 5. Configure DMA interrupt generation after half/ full transfer as required by the
			// application.
			
			//
			// 6. Clear the TC flag in the USART_ISR register by setting the TCCF bit in the
			// USART_ICR register.
			IFCR().CGIF() = true;
			//
			// 7. Activate the channel in the DMA register.
			CCR().EN() = true;
		}
		/**
		 * @brief Restarts a DMA trasaction without changing the memory location.
		 * 
		 * @param count The number of items to transmit.
		 */
		static void Restart( std::size_t count ) noexcept
		{
			Disable();
			CNDTR().NDT() = count;
			IFCR().CGIF() = true;
			CCR().EN() = true;
		}
		/**
		 * @brief Restarts a DMA trasaction with the same memory and count.
		 */
		static void Restart() noexcept
		{
			Disable();
			Enable();
		}
	};
	/**
	 * @brief A manager class of the DMA module.
	 * 
	 * @tparam Function The callback to call for the interrupt.
	 * @tparam Module The DMA module number.
	 * @tparam Channel The DMA channel number.
	 * @tparam Priority The priority of the interrupt.
	 */
	template <typename Function, std::size_t Module, std::size_t Channel, std::size_t Priority = 2>
	class DMAModule : public DMAProperties<Module, Channel, Priority>
	{
	public:
		using base_t 		= DMAProperties<Module, Channel, Priority>;
		using kernal_t 		= DMAPowerKernal<Channel, Priority>;
		using power_t 		= General::ModulePower<kernal_t>;
		using interrupt_t 	= System::Interrupt<DMAModule, DMAChannelInterrupt<Channel>()>;
		using callback_t 	= General::StaticLambdaWrapper<Function>;
		//
		//
		using REGS			= DMAGeneral::Registers<Module, Channel>;
		using CCR  			= typename REGS::CCR_t;
		using CPAR 			= typename REGS::CPAR_t;
		using CMAR 			= typename REGS::CMAR_t;
		using CNDTR			= typename REGS::CNDTR_t;
		using ISR 			= typename REGS::ISR_t;
		using IFCR 			= typename REGS::IFCR_t;
		//
		//
	private:
		power_t 		m_PSR;
		interrupt_t		m_ISR;
		callback_t 		m_Callback;
		//
		static void Reset() noexcept
		{
			CCR().EN().Set( false );
			CCR().Set( 0 );
			CNDTR().Set( 0 );
			CPAR().Set( 0 );
			CMAR().Set( 0 );
			IFCR().Clear();
		}
	public:
		/**
		 * @brief Construct a new DMAModule object
		 * 
		 * @tparam Callback The callback type to run for each interrupt event.
		 * @tparam Peripheral_T A pointer type to the peripheral.
		 * @tparam Memory_T A pointer type to the memory.
		 * @param callback The callback to run for interrupts.
		 * @param peripheral The pointer to the peripheral register.
		 * @param memory The pointer to the memory location.
		 * @param count The number of items in the DMA.
		 * @param direction The direction of the DMA transactions.
		 * @param priority The priority of the DMA transaction.
		 * @param circular Whether or not the transaction is circular.
		 * @param interrupt_on_error Whether or not to interrupt on error.
		 * @param interrupt_on_complete Whether or not to interrupt on dma completion.
		 * @param interrupt_on_half_complete Whether or not to interrupt on dma transaction half complete.
		 * @param default_enabled Whether or not to immediately enable the DMA.
		 * @param memory_increment Whether or not the memory pointer is incremented each transaction.
		 * @param peripheral_increment Whether or not the peripheral pointer is incremented each transaction.
		 */
		template <typename Callback, typename Peripheral_T, typename Memory_T>
		DMAModule
		(
			DMAProperties<Module, Channel, Priority>, 
			Callback &&						callback,
			Peripheral_T * 					peripheral, 
			Memory_T * 						memory,
			std::size_t  					count,
			DMAGeneral::Direction const 	direction,
			DMAGeneral::Priority const 		priority 					= DMAGeneral::Priority::Low,
			bool const 						circular 					= false,
			bool const 						interrupt_on_error 			= true, 
			bool const 						interrupt_on_complete 		= true,
			bool const						interrupt_on_half_complete	= false,
			bool const						default_enabled				= true,
			bool const 						memory_increment			= true,
			bool const 						peripheral_increment		= false
		)
		noexcept : m_Callback{ std::forward<Callback>( callback ) }
		{
			Reset();
			//
			static_assert( sizeof(Memory_T) <= sizeof(Peripheral_T), "Memory block sizes for DMA transfer cannot fit peripheral content." );
			CCR().EN() 			= false;
			//
			// Configuration
			CCR().MEM2MEM()		= ( direction == DMAGeneral::Direction::MemoryToMemory );
			CNDTR().NDT()		= count;
			CCR().CIRC()		= circular;
			CCR().DIR()			= (std::uint32_t)direction;
			CCR().PL()  		= (std::uint32_t)priority;
			//
			// Memory settings
			CMAR().MA() 		= (std::uint32_t)memory;
			CCR().MINC() 		= memory_increment;
			CCR().MSIZE() 		= (std::uint32_t)base_t::template GetMemorySize<sizeof(Memory_T)>();
			//
			// Peripheral settings
			CPAR().PA() 		= (std::uint32_t)peripheral;
			CCR().PINC() 		= peripheral_increment;
			CCR().PSIZE()		= (std::uint32_t)base_t::template GetMemorySize<sizeof(Peripheral_T)>();
			//
			// Interrupt settings
			CCR().TEIE()		= interrupt_on_error;
			CCR().TCIE()		= interrupt_on_complete;
			CCR().HTIE() 		= interrupt_on_half_complete;
			//
			// Enable DMA
			if (default_enabled) CCR().EN() = true;
		}
		/**
		 * @brief The interrupt callback for the DMA module.
		 * 
		 */
		static void Interrupt() noexcept
		{
			if ( ISR().GIF() )
			{
				//
				// Normal handling
				if ( CCR().HTIE() and ISR().HTIF() )
				{
					callback_t::Run( DMAInterruptFlag::HalfComplete );
					IFCR().CHTIF() = true;
				}
				if ( CCR().TCIE() and ISR().TCIF() )
				{
					callback_t::Run( DMAInterruptFlag::Complete );
					IFCR().CTCIF() = true;
				}
				//
				// Error handling
				if ( ISR().TEIF() )
				{
					callback_t::Run( DMAInterruptFlag::Error );
					IFCR().CTEIF() = true;
				}
				//
				// Clear the global interrupt flag
				IFCR().CGIF() = true;
			}
		}
	};
	/**
	 * @brief A deduction guide for the DMA module.
	 * 
	 * @tparam M 
	 * @tparam C 
	 * @tparam P 
	 * @tparam F 
	 * @tparam Args 
	 */
	template <size_t M, size_t C, size_t P, typename F, typename ... Args>
	DMAModule(DMAProperties<M, C, P>, F, Args...) -> DMAModule<F, M, C, P>;
}