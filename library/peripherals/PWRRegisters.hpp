#pragma once
#include "Meta.hpp"
#include "Register.hpp"
#include "stm32f0xx.h"
#include <cstdint>

namespace Peripherals::PWRGeneral
{
	/**
	 * @brief The control register for the PWR peripheral.
	 */
    struct CR : public General::u32_reg<PWR_BASE + offsetof(PWR_TypeDef, CR)>
    {
		using base_t = General::u32_reg<PWR_BASE + offsetof(PWR_TypeDef, CR)>;
		using base_t::base_t;
		/**
		 * @brief Disable RTC domain write protection.
		 * 
		 * @return auto 
		 */
		auto DBP()  { return base_t::template Actual<PWR_CR_DBP>();  }
		/**
		 * @brief Clear standby flag.
		 * 
		 * @return auto 
		 */
		auto CSBF()	{ return base_t::template Actual<PWR_CR_CSBF>(); }
		/**
		 * @brief Clear wakeup flag.
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
		auto CWUF()	{ return base_t::template Actual<PWR_CR_CWUF>(); }
		/**
		 * @brief Power down deep-sleep.
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
		auto PDDS()	{ return base_t::template Actual<PWR_CR_PDDS>(); }

		/**
		 * @brief PVD level selection.
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
		auto PLS()  { return base_t::template Actual<PWR_CR_PLS>();  }
		/**
		 * @brief Power voltage detector enable.
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
		auto PVDE() { return base_t::template Actual<PWR_CR_PVDE>(); }
		/**
		 * @brief Clear standby flag.
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
		auto LPDS()	{ return base_t::template Actual<PWR_CR_LPDS>(); }
    };
	/**
	 * @brief The control register for the PWR peripheral.
	 */
    struct CSR : public General::u32_reg<PWR_BASE + offsetof(PWR_TypeDef, CSR)>
    {
		using base_t = General::u32_reg<PWR_BASE + offsetof(PWR_TypeDef, CSR)>;
		using base_t::base_t;
		/**
		 * @brief Wakeup flag
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto WUF        () { return base_t::template Actual<PWR_CSR_WUF>(); }
		/**
		 * @brief  Standby flag
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto SBF        () { return base_t::template Actual<PWR_CSR_SBF>(); }
		/**
		 * @brief PVD output
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto PVDO       () { return base_t::template Actual<PWR_CSR_PVDO>(); }
		/**
		 * @brief VREFINT reference voltage ready
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto VREFINTRDY () { return base_t::template Actual<PWR_CSR_VREFINTRDY>(); }
		/**
		 * @brief Enable WKUP1 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP1      () { return base_t::template Actual<PWR_CSR_EWUP1>(); }
		/**
		 * @brief Enable WKUP2 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP2      () { return base_t::template Actual<PWR_CSR_EWUP2>(); }
		/**
		 * @brief Enable WKUP3 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP3      () { return base_t::template Actual<PWR_CSR_EWUP3>(); }
		/**
		 * @brief Enable WKUP4 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP4      () { return base_t::template Actual<PWR_CSR_EWUP4>(); }
		/**
		 * @brief Enable WKUP5 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP5      () { return base_t::template Actual<PWR_CSR_EWUP5>(); }
		/**
		 * @brief Enable WKUP6 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP6      () { return base_t::template Actual<PWR_CSR_EWUP6>(); }
		/**
		 * @brief Enable WKUP7 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP7      () { return base_t::template Actual<PWR_CSR_EWUP7>(); }
		/**
		 * @brief Enable WKUP8 pin
		 * 
		 * @return auto Bitfield class for using this bit.
		 */
        auto EWUP8      () { return base_t::template Actual<PWR_CSR_EWUP8>(); }
    };
}