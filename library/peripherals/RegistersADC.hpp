#pragma once

#include "Meta.hpp"
#include "Pin.hpp"
#include "Register.hpp"
#include "Meta.hpp"
#include <cstdint>

namespace Peripherals::ADCGeneral
{	
	enum class Flags : std::uint32_t
	{
		ADRDY			= 0u,
		EOSMP 			= 1u,
		EOC 			= 2u,
		EOSEQ 			= 3u,
		OVR 			= 4u,
		AWD 			= 7u
	};
	
	enum class ExternalTriggerPolarity : std::uint32_t
	{
		None 		= 0b00,
		RisingEdge 	= 0b01,
		FallingEdge = 0b10,
		BothEdges 	= 0b11
	};
	enum class ExternalTriggerSource : std::uint32_t
	{
		TRG0 = 0u,
		TRG1 = 1u,
		TRG2 = 2u,
		TRG3 = 3u,
		TRG4 = 4u,
		TRG5 = 5u,
		TRG6 = 6u,
		TRG7 = 7u
	};
	enum class Resolution : std::uint32_t
	{
		Bits12			= 0b00,
		Bits10			= 0b01,
		Bits8			= 0b10,
		Bits6			= 0b11
	};
	enum class ADCClockMode : std::uint32_t
	{
		ADCCLK 			= 0b00,
		PCLK_2 			= 0b01,
		PCLK_4 			= 0b10
	};
	enum class ADCClockSampling : std::uint32_t
	{
		CLK_1_5			= 0b000,
		CLK_7_5			= 0b001,
		CLK_13_5		= 0b010,
		CLK_28_5		= 0b011,
		CLK_41_5		= 0b100,
		CLK_55_5		= 0b101,
		CLK_71_5		= 0b110,
		CLK_239_5		= 0b111
	};
	enum class ADCModules : std::uint32_t
	{
		VBAT 			= 24u,
		TEMP 			= 23u,
		VREF 			= 22u
	};
	enum class OverrunMode : bool
	{
		KeepOld			= false,
		OverrideOld		= true
	};
	enum class Alignment : bool
	{
		MSB 			= true,
		LSB 			= false
	};
	enum class Direction : bool
	{
		Upward 			= false,
		Backward 		= true
	};
	enum class DMAMode : bool
	{
		OneShot			= false,
		CircularMode	= true
	};

	template <std::size_t A>
	struct CR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto ADCAL()
		{
			return base_t::template Actual<ADC_CR_ADCAL>();
		}
		auto ADSTP()
		{
			return base_t::template Actual<ADC_CR_ADSTP>();
		}
		auto ADSTART()
		{
			return base_t::template Actual<ADC_CR_ADSTART>();
		}
		auto ADDIS()
		{
			return base_t::template Actual<ADC_CR_ADDIS>();
		}
		auto ADEN()
		{
			return base_t::template Actual<ADC_CR_ADEN>();
		}
	};
	
	template <std::size_t A>
	struct CFGR1 : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto AWDCH()
		{
			return base_t::template Actual<ADC_CFGR1_AWDCH>();
		}
		auto AWDEN()
		{
			return base_t::template Actual<ADC_CFGR1_AWDEN>();
		}
		auto AWDSGL()
		{
			return base_t::template Actual<ADC_CFGR1_AWDSGL>();
		}
		auto DISCEN()
		{
			return base_t::template Actual<ADC_CFGR1_DISCEN>();
		}
		auto AUTOFF()
		{
			return base_t::template Actual<ADC_CFGR1_AUTOFF>();
		}
		auto WAIT()
		{
			return base_t::template Actual<ADC_CFGR1_WAIT>();
		}
		auto CONT()
		{
			return base_t::template Actual<ADC_CFGR1_CONT>();
		}
		auto OVRMOD()
		{
			return base_t::template Actual<ADC_CFGR1_OVRMOD>();
		}
		auto EXTEN()
		{
			return base_t::template Actual<ADC_CFGR1_EXTEN>();
		}
		auto EXTSEL()
		{
			return base_t::template Actual<ADC_CFGR1_EXTSEL>();
		}
		auto ALIGN()
		{
			return base_t::template Actual<ADC_CFGR1_ALIGN>();
		}
		auto RES()
		{
			return base_t::template Actual<ADC_CFGR1_RES>();
		}
		auto SCANDIR()
		{
			return base_t::template Actual<ADC_CFGR1_SCANDIR>();
		}
		auto DMACFG()
		{
			return base_t::template Actual<ADC_CFGR1_DMACFG>();
		}
		auto DMAEN()
		{
			return base_t::template Actual<ADC_CFGR1_DMAEN>();
		}
	
		void ContiniousMode(bool Enabled = true)
		{
			CONT() = Enabled;
		}
		void DiscontinousMode(bool Enabled = true)
		{
			DISCEN() = Enabled;
		}
		void AutoOffMode(bool Enabled = true)
		{
			AUTOFF() = Enabled;
		}
		void WaitMode(bool const Enabled = true)
		{
			WAIT() = Enabled;
		}
		void Alignment(Alignment const pInput)
		{
			ALIGN() = (std::uint32_t)pInput;
		}
		void OverrunManagementMode(OverrunMode const Mode = OverrunMode::OverrideOld)
		{
			OVRMOD() = (std::uint32_t)Mode;
		}
		void Resolution( Resolution const pInput )
		{
			ALIGN() = (std::uint32_t)pInput;
		}
		void ScanDirection(Direction const pInput)
		{
			SCANDIR() = (std::uint32_t)pInput;
		}

		void ExternalTrigger(ExternalTriggerPolarity const pPolarity, ExternalTriggerSource const pSource)
		{
			EXTEN() = (std::uint32_t)pPolarity;
			EXTSEL() = (std::uint32_t)pSource;
		}
		void DMAConfiguration(bool const pEnabled, DMAMode const pMode)
		{
			DMAEN() = pEnabled;
			DMACFG() = (std::uint32_t)pMode;
		}
	};
	
	template <std::size_t A>
	struct IER : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		template<typename ... ISR_t>
		void Enable( ISR_t && ... pISR )
		{
			base_t::operator =(General::MultiMaskBit( ((std::uint32_t)pISR) ... ));
		}
	};
	
	template <std::size_t A>
	struct ISR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto AWD()
		{
			return base_t::template Actual<ADC_ISR_AWD>();
		}
		auto OVR()
		{
			return base_t::template Actual<ADC_ISR_OVR>();
		}
		auto EOSEQ()
		{
			return base_t::template Actual<ADC_ISR_EOSEQ>();
		}
		auto EOC()
		{
			return base_t::template Actual<ADC_ISR_EOC>();
		}
		auto EOSMP()
		{
			return base_t::template Actual<ADC_ISR_EOSMP>();
		}
		auto ADRDY()
		{
			return base_t::template Actual<ADC_ISR_ADRDY>();
		}
	};
	
	template <std::size_t A>
	struct CFGR2 : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto CKMODE()
		{
			return base_t::template Actual<ADC_CFGR2_CKMODE>();
		}
		void SetClockMode(ADCClockMode const pMode)
		{
			CKMODE() = (std::uint32_t)pMode;
		}
	};
	
	template <std::size_t A>
	struct SMPR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto SMP()
		{
			return base_t::template Actual<ADC_SMPR_SMP>();
		}
		
		void SetSamplingTime(ADCClockSampling const pSampling)
		{
			SMP() = (std::uint32_t)pSampling;
		}
	};
	
	template <std::size_t A>
	struct TR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	};
	
	template <std::size_t A>
	struct CHSELR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		template <typename ... Channels>
		void Select(Channels... pInput)
		{
			base_t::operator =(General::MultiMaskBit<std::uint32_t>( pInput ... ));
		}
	};
	
	template <std::size_t A>
	struct DR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
	};
	
	template <std::size_t A>
	struct CCR : public General::u32_reg<A>
	{
		using base_t = General::u32_reg<A>;
		using base_t::base_t;
		
		auto TSEN() 	{ return base_t::template Actual<ADC_CCR_TSEN>(); 	}
		auto VBATEN() 	{ return base_t::template Actual<ADC_CCR_VBATEN>(); 	}
		auto VREFEN() 	{ return base_t::template Actual<ADC_CCR_VREFEN>(); 	}
	};
		
	CR() 		-> CR		<ADC1_BASE + offsetof(ADC_TypeDef, 			CR)		>;
	CFGR1() 	-> CFGR1	<ADC1_BASE + offsetof(ADC_TypeDef, 			CFGR1)	>;
	IER() 		-> IER		<ADC1_BASE + offsetof(ADC_TypeDef, 			IER)	>;
	ISR() 		-> ISR		<ADC1_BASE + offsetof(ADC_TypeDef, 			ISR)	>;
	CFGR2() 	-> CFGR2	<ADC1_BASE + offsetof(ADC_TypeDef, 			CFGR2)	>;
	SMPR() 		-> SMPR		<ADC1_BASE + offsetof(ADC_TypeDef, 			SMPR)	>;
	TR() 		-> TR		<ADC1_BASE + offsetof(ADC_TypeDef, 			TR)		>;
	CHSELR() 	-> CHSELR	<ADC1_BASE + offsetof(ADC_TypeDef, 			CHSELR)	>;
	DR() 		-> DR		<ADC1_BASE + offsetof(ADC_TypeDef, 			DR)		>;
	CCR() 		-> CCR		<ADC_BASE  + offsetof(ADC_Common_TypeDef, 	CCR)	>;
	
	std::uint16_t Calibrate()
	{
		if (CR{}.ADEN())
		{
			CR{}.ADDIS() = true;
			while (CR{}.ADEN());
		}
		
		CR{}.ADCAL() = true;
		while (CR{}.ADCAL());
		return 0x7f & DR{}.Get();
	}
	
	void EnableSequence()
	{
		if (ISR{}.ADRDY())
			ADC1->ISR |= ADC_ISR_ADRDY;
		CR{}.ADEN() = true;
		while (!ISR{}.ADRDY());
	}
	
	void DisableSequence()
	{
		//Stop ADC and wait for stopped.
		CR{}.ADSTP() |= true;
		while (CR{}.ADSTP());
		
		//Disable ADC and wait for enable to clear.
		CR{}.ADDIS() |= true;
		while (CR{}.ADDIS());
	}
}