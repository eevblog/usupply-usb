#pragma once
#include "Types.hpp"
#include "Register.hpp"
#include "stm32f0xx.h"
#include <cstdint>

namespace Peripherals::SysCfgGeneral
{
    /**
     * 
     */
    using CFGR1_REG     = General::u32_reg<SYSCFG_BASE + 0x00>;
    /**
     * 
     */
    template <unsigned N>
    using EXTICR_REG    = General::u32_reg<SYSCFG_BASE + 0x08 + 4u * (N - 1u)>;
    /**
     * 
     */
    using CFGR2_REG     = General::u32_reg<SYSCFG_BASE>;
    /**
     * CFGR1 Definition
     */
    struct CFGR1 : CFGR1_REG
    {
        using base_t = CFGR1_REG;
        using base_t::base_t;
        /**
         * 
         */
        auto MEM_MODE         () { return base_t::template Actual<SYSCFG_CFGR1_MEM_MODE           >(); }
        auto IRDA_ENV_SEL     () { return base_t::template Actual<SYSCFG_CFGR1_IRDA_ENV_SEL       >(); }
        auto IRDA_ENV_SEL_0   () { return base_t::template Actual<SYSCFG_CFGR1_IRDA_ENV_SEL_0     >(); }
        auto IRDA_ENV_SEL_1   () { return base_t::template Actual<SYSCFG_CFGR1_IRDA_ENV_SEL_1     >(); }
        auto PA11_PA12_RMP    () { return base_t::template Actual<SYSCFG_CFGR1_PA11_PA12_RMP      >(); }
        auto ADC_DMA_RMP      () { return base_t::template Actual<SYSCFG_CFGR1_ADC_DMA_RMP        >(); }
        auto USART1TX_DMA_RMP () { return base_t::template Actual<SYSCFG_CFGR1_USART1TX_DMA_RMP   >(); }
        auto USART1RX_DMA_RMP () { return base_t::template Actual<SYSCFG_CFGR1_USART1RX_DMA_RMP   >(); }
        auto TIM16_DMA_RMP    () { return base_t::template Actual<SYSCFG_CFGR1_TIM16_DMA_RMP      >(); }
        auto TIM17_DMA_RMP    () { return base_t::template Actual<SYSCFG_CFGR1_TIM17_DMA_RMP      >(); }
        auto TIM16_DMA_RMP2   () { return base_t::template Actual<SYSCFG_CFGR1_TIM16_DMA_RMP2     >(); }
        auto TIM17_DMA_RMP2   () { return base_t::template Actual<SYSCFG_CFGR1_TIM17_DMA_RMP2     >(); }
        auto I2C_FMP_PB6      () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PB6        >(); }
        auto I2C_FMP_PB7      () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PB7        >(); }
        auto I2C_FMP_PB8      () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PB8        >(); }
        auto I2C_FMP_PB9      () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PB9        >(); }
        auto I2C_FMP_I2C1     () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_I2C1       >(); }
        auto I2C_FMP_I2C2     () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_I2C2       >(); }
        auto I2C_FMP_PA9      () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PA9        >(); }
        auto I2C_FMP_PA10     () { return base_t::template Actual<SYSCFG_CFGR1_I2C_FMP_PA10       >(); }
        auto SPI2_DMA_RMP     () { return base_t::template Actual<SYSCFG_CFGR1_SPI2_DMA_RMP       >(); }
        auto USART2_DMA_RMP   () { return base_t::template Actual<SYSCFG_CFGR1_USART2_DMA_RMP     >(); }
        auto USART3_DMA_RMP   () { return base_t::template Actual<SYSCFG_CFGR1_USART3_DMA_RMP     >(); }
        auto I2C1_DMA_RMP     () { return base_t::template Actual<SYSCFG_CFGR1_I2C1_DMA_RMP       >(); }
        auto TIM1_DMA_RMP     () { return base_t::template Actual<SYSCFG_CFGR1_TIM1_DMA_RMP       >(); }
        auto TIM2_DMA_RMP     () { return base_t::template Actual<SYSCFG_CFGR1_TIM2_DMA_RMP       >(); }
        auto TIM3_DMA_RMP     () { return base_t::template Actual<SYSCFG_CFGR1_TIM3_DMA_RMP       >(); }
    };
    /**
     * EXTICR Definition
     */
    template<unsigned N>
    struct EXTICR : EXTICR_REG<N>
    {
        using base_t = EXTICR_REG<N>;
        using base_t::base_t;
        /**
         * 
         */
        template<unsigned C>
        static constexpr bool Valid() noexcept
        {
            switch (C)
            {
            case 0:     return N==1;
            case 1:     return N==1;
            case 2:     return N==1;
            case 3:     return N==1;
            case 4:     return N==2;
            case 5:     return N==2;
            case 6:     return N==2;
            case 7:     return N==2;
            case 8:     return N==3;
            case 9:     return N==3;
            case 10:    return N==3;
            case 11:    return N==3;
            case 12:    return N==4;
            case 13:    return N==4;
            case 14:    return N==4;
            case 15:    return N==4;
            default:    return false;
            }
        }
        template <unsigned C>
        static constexpr uint32_t Mask() noexcept
        {
            switch( C & 0b11 )
            {
            case 0:     return 0x000fu;
            case 1:     return 0x00f0u;
            case 2:     return 0x0f00u;
            case 3:     return 0xf000u;
            }
        }
        /**
         * 
         */
        template <unsigned C>
        auto EXTI_C()
        {
            static_assert(Valid<C>(), "Not a valid EXTI field, maybe you want a differnt register?");
            return base_t::template Actual<Mask<C>()>();
        }
    };
    /**
     * CFGR2 Definition
     */
    struct CFGR2 : CFGR2_REG
    {
        using base_t = CFGR2_REG;
        using base_t::base_t;
        /**
         * 
         */
        auto LOCKUP_LOCK      () { return base_t::template Actual<SYSCFG_CFGR2_LOCKUP_LOCK       >(); }       
        auto SRAM_PARITY_LOCK () { return base_t::template Actual<SYSCFG_CFGR2_SRAM_PARITY_LOCK  >(); }   
        auto PVD_LOCK         () { return base_t::template Actual<SYSCFG_CFGR2_PVD_LOCK          >(); }           
        auto SRAM_PEF         () { return base_t::template Actual<SYSCFG_CFGR2_SRAM_PEF          >(); }           
    };
}