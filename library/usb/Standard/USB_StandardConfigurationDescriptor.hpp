#pragma once
#include "USB_Types.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief A helper class that prevents invalid attributes when used exclusively.
     */
    struct RemoteWakeupTag { static constexpr U<1> value = 0b00100000; }    RemoteWakeup;
    /**
     * @brief A helper class that prevents invalid attributes when used exclusively.
     */
    struct SelfPoweredTag  { static constexpr U<1> value = 0b01000000; }    SelfPowered;
    /**
     * @brief A helper class that prevents invalid attributes when used exclusively.
     */
    struct BusPoweredTag  { static constexpr U<1> value = 0b00000000; }     BusPowered;
    /**
     * @brief A class for the attribute field in a USB descriptor.
     */
    struct Attributes
    {
        U<1> Value;
        //
        template <typename ... Args>
        constexpr Attributes( Args ... ) noexcept : Value{ 0b10000000 | (Args::value | ...) } {}
    };
    /**
     * @brief A standard configuration descriptor.
     * 
     * @note See usb 2.0 specifications.
     */
    struct StandardConfigurationDescriptor : StandardHeader<StandardConfigurationDescriptor, DescriptorTypes::CONFIGURATION>
    {
        U<2>        wTotalLength;
        U<1>        bNumInterfaces;
        U<1>        bConfigurationValue;
        U<1>        iConfiguration;
        Attributes  bmAttributes;
        MaxPower    bMaxPower;
    };
    /**
     * @brief This is the user configurable standard configuration descriptor fields
     * 
     * This is a more typesafe version of a raw configuration descriptor.
     */
    struct AutoConfiguration
    {
        U<1>        bConfigurationValue;
        U<1>        iConfiguration;
        Attributes  bmAttributes;
        MaxPower    bMaxPower;
    };
    #pragma pack(pop)
}