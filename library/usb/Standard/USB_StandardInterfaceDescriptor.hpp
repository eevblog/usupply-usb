#pragma once
#include "Meta.hpp"
#include "USB_Types.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief The supported interface classes.
     * 
     * @note These should be added as more classes are added.
     */
    enum class InterfaceClass : U<1>
    {
        CDC = 0x02,
        HID = 0x03
    };
    /**
     * @brief The CDC subclasses.
     * 
     * @note See usb CDC specifications.
     */
    enum class CDCSubClassCodes : U<1>
    {
        DirectLineControlModel              = 0x01,
        AbstractControlModel                = 0x02,
        TelephoneControlModel               = 0x03,
        MultiChannelControlModel            = 0x04,
        CAPIControlModel                    = 0x05,
        EthernetNetworkingControlModel      = 0x06,
        ATMNetworkingControlModel           = 0x07,
        WirelessHandsetControlModel         = 0x08,
        DeviceManagement                    = 0x09,
        MobileDirectLineModel               = 0x0A,
        OBEX                                = 0x0B,
        EthernetEmulationModel              = 0x0C,
        NetworkControlModel                 = 0x0D,
        MobileBroadbandInterfaceModel       = 0x0E
    };
    /**
     * @brief The CDC protocol codes.
     * 
     * @note See usb CDC specifications.
     */
    enum class CDCClassProtocolCodes : U<1>
    {
        None             = 0x00,
        ATCommands       = 0x01,
        AT_ITUT_V250     = 0x01,
        AT_PCCA101       = 0x02,
        AT_PCCA101AnnexO = 0x03,
        AT_GSM7_07       = 0x04,
        AT_3GPP27_07     = 0x05,
        AT_CS0017_0      = 0x06,
        EEM              = 0x07,
        Vendor           = 0xFF,
    };
    /**
     * @brief The common conguration for an interface descriptor.
     * 
     * @tparam Interface The interface class.
     * @tparam Child The class that inherits from BaseInterfaceDescriptor (CRTP).
     */
    template <InterfaceClass Interface, typename Child>
    struct BaseInterfaceDescriptor : StandardHeader<Child, DescriptorTypes::INTERFACE>
    {
        U<1>                    bInterfaceNumber    = 0;
        U<1>                    bAlternateSetting   = 0;
        U<1>                    bNumEndpoints       = static_cast<U<1>>( Child::EndpointCount );
        InterfaceClass const    bInterfaceClass     = Interface;
        /**
         */
        constexpr BaseInterfaceDescriptor( U<1> number ) noexcept : bInterfaceNumber{ number } {}
    };
    /**
     * @brief A CDC descriptor base, common to cdc descprtors.
     * 
     * @tparam Count The number of endpoints for the interface.
     */
    template <std::size_t Count>
    struct CDCDescriptorBase : BaseInterfaceDescriptor<InterfaceClass::CDC, CDCDescriptorBase<Count>>
    {
        static constexpr std::size_t EndpointCount = Count;
        /**
         */
        CDCSubClassCodes        bInterfaceSubClass;
        CDCClassProtocolCodes   bInterfaceProtocol;
        U<1>                    iInterface;
    };
    /**
     * @brief A cdc interface class which gnerates the nessary information from the inherited endpoints.
     * 
     * @tparam Endpoints The endpoints used by the CDC interface.
     */
    template <typename ... Endpoints>
    struct AutoCDCInterface :
        CDCDescriptorBase<sizeof...(Endpoints)>, 
        UniqueArgs<Endpoints...>
    {
        using base_t = CDCDescriptorBase<sizeof...(Endpoints)>;
        /**
         * @brief 
         */
        constexpr AutoCDCInterface( U<1> number, CDCSubClassCodes sub_class, CDCClassProtocolCodes interface_protocol, U<1> interface, Endpoints ... endpoints ) noexcept :
            base_t          { { number }, sub_class, interface_protocol, interface },
            UniqueArgs<Endpoints...>{ endpoints ... }
        {}
    };
    #pragma pack(pop)
}