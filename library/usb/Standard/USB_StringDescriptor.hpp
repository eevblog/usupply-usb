#pragma once
#include "USB_Types.hpp"
#include "USB_StandardDeviceRequest.hpp"
#include "USB_Languages.hpp"
#include <array>

namespace USB
{
    #pragma pack(push, 1)
    template<size_t N>
    /**
     * @brief A string descriptor with automatic encoding from utf8 to utf16 at compile time.
     */
    struct StringDescriptor : StandardHeader<StringDescriptor<N>, DescriptorTypes::STRING>
    {
        using array_t = std::array<std::remove_cv_t<U<2>>, N>;
        array_t const bString;
        /**
         * @brief Construct a string descriptr from a string literal.
         * @note This converts the UTF8 to UTF16 (compile-time)
         */
        constexpr StringDescriptor( const char ( & input )[ N + 1u ] ) noexcept :
            bString
            {
                [&]() constexpr -> array_t
                {
                    array_t output{};
                    for(std::size_t i = 0; i < N; ++i)
                    {
                        (output[i] = input[i]) <<= 0;
                    }
                    return output;
                }()
            }
        {}
    };
    /**
     * @brief Deduce the string descriptor template arguments from the input string.
     */
    template <size_t N>
    StringDescriptor( const char (&)[N] ) -> StringDescriptor<N - 1>;
    /**
     * @brief The string desciptor zero is the language descriptor.
     * 
     * @note See usb 2.0 specifications.
     */
    template<size_t N>
    struct StringDescriptorZero : StandardHeader<StringDescriptorZero<N>, DescriptorTypes::STRING>
    {
        LanguageID wLANGID[N];
        /**
         * @brief Construct a language descriptor from the input language ID's
         */
        template <typename ... Args>
        constexpr StringDescriptorZero( Args ... args ) noexcept :
            wLANGID{ args ... }
        {}
    };
    /**
     * @brief Deduce the language descriptor language count from the input languages.
     */
    template <typename ... Args>
    StringDescriptorZero( Args ... args ) -> StringDescriptorZero< sizeof...(Args) >;
    #pragma pack(pop)
}