#include "Types.hpp"
#include "I2CBitbanging.hpp"
#include "platform_inc.h"
#include "PinDefinitions.hpp"

volatile bool TypeCMode = false;

extern "C"
{
    void uart_prints( const char *str,const uint8_t len)
    {
    }
    void irq_lock(void)
    {
        //__enable_irq();
    }
    void irq_unlock(void)
    {
        //__disable_irq();d
    }
    bool plat_i2c_read_block(uint8_t slave,uint8_t reg, uint8_t count, uint8_t * data)
    {
        return ::System::RT1716::Mixed( IO::Address<7>{ slave }, USB::RBytes( reg ), USB::ReceiveData{ data, count } );
    }
    bool plat_i2c_write_block(uint8_t slave,uint8_t reg, uint8_t count, uint8_t * data)
    {
        return ::System::RT1716::Mixed( IO::Address<7>{ slave }, USB::RBytes( reg ), USB::TransmitData{ data, count } );
    }
    bool plat_check_alert(void)
    {
        return true;
    }
    bool plat_check_vdc_in(void)
    {
        return true;
    }
    bool plat_check_self_power(void)
    {
        return true;
    }
    void plat_notify_tcpm_status(uint8_t status)
    {
        if(status == NOTIFY_TCPM_STATUS_READY){
            TypeCMode = true;
        }
    }
    void plat_power_source_enable(uint8_t en)
    {
    }
    void plat_power_sink_enable(uint8_t en)
    {
	 	::System::Pins::PWR_EN::Write( !!en );
    }
    void plat_power_bulk_enable(uint8_t en)
    {
    }
    void plat_power_discharge_enable(uint8_t en)
    {
    }
    void plat_notify_power_change()
    {
    }
}

namespace USB::PD
{
    using TCPC              = System::RT1716;
    using TCPC_I2C          = System::RT1716_I2C;
    //
    static volatile bool ready = false;
    void Tick() noexcept
    {   
        Timer_1Ms_Handler();
    }
    void Start() noexcept
    {
	    static TCPC_I2C usb_pd;
	    PD_Reset_Init();
    }
    void Run() noexcept
    {
        PD_Run_State_Machine();
    }
}