/*     Copyright <2017> <Richtek Technology Corp.>
        Permission is hereby granted, free of charge, to any person obtaining a copy of
        this source code, library, and associated documentation files( the "Software"),
        to deal in the source code without restriction, including without limitation the
        rights to use, copy, modify, merge, publish, distribute copies of the
        Software, and to permit persons to whom the Software is furnished to do so,
        subject to the following conditions:
        The Software is restricted to use with Richtek's USB PD products or evaluation kits.
        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
        FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
        COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
        IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
        CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.*/

#include "platform_inc.h"
#include "pd_export_def.h"

#define PD_MIDB_MIN_SIZE	4
#define PD_MIDB_MAX_SIZE	26
#define PD_MIDB_DYNAMIC_SIZE	(PD_MIDB_MAX_SIZE-PD_MIDB_MIN_SIZE)

typedef struct {
	uint8_t	vid[2];
	uint8_t	pid[2];
	char	mfrs_string[PD_MIDB_DYNAMIC_SIZE];
}pd_manufacturer_info_t;


#define	DEFAULT_SRC_CAP_CNT						1
#define	DEFAULT_SNK_CAP_CNT						4

/********************************************************************************/
/*						Source/Sink Capabilities of Port 0								*/
/********************************************************************************/


#define	PORT0_DUAL_ROLE_POWER				0
#define	PORT0_DATA_ROLE_SWAP				0
#define	PORT0_USB_SUSPEND_SUPPORTED			0
#define	PORT0_EXTERNALLY_POWERED			0
#define	PORT0_USB_COMMUNICATION_CAPABLE		0
#define	PORT0_HIGH_CAPABILITY				0



/********************************************************************************/
/*					Flags Composition of the Source/Sink Capabilities 					*/
/********************************************************************************/

#define	USER_PORT0_SRC_CAP_PDO_0_FLAGS			\
			((PORT0_DUAL_ROLE_POWER<<4)+(PORT0_USB_SUSPEND_SUPPORTED<<3)+(PORT0_EXTERNALLY_POWERED<<2)+(PORT0_USB_COMMUNICATION_CAPABLE<<1)+PORT0_DATA_ROLE_SWAP)

#define	USER_PORT0_SNK_CAP_PDO_0_FLAGS			\
			((PORT0_DUAL_ROLE_POWER<<4)+(PORT0_USB_SUSPEND_SUPPORTED<<3)+(PORT0_EXTERNALLY_POWERED<<2)+(PORT0_USB_COMMUNICATION_CAPABLE<<1)+PORT0_DATA_ROLE_SWAP)


uint8_t  slave_base_addr=0x4E;


xu8 typec_role[1]= {TYPEC_ROLE_DRP};




const uint8_t  src_cap_cnt[1]= {DEFAULT_SRC_CAP_CNT};
const uint8_t  src_default_caps_tbl[1][28] =
{
    {
         SRC_CAP_5V_PDO (5000, 3000,USER_PORT0_SRC_CAP_PDO_0_FLAGS)
        ,SRC_CAP_FIX_PDO(9000, 3000)
        ,SRC_CAP_FIX_PDO(12000, 3000)
        ,SRC_CAP_FIX_PDO(15000, 2400)
    }
};



uint8_t snk_cap_cnt[1]= {DEFAULT_SNK_CAP_CNT};
uint8_t snk_default_caps_tbl_le[1][28] =
{
    {
        SNK_CAP_5V_PDO(5000, 1500,USER_PORT0_SNK_CAP_PDO_0_FLAGS)
		,SNK_CAP_FIX_PDO(9000,2000)
		,SNK_CAP_FIX_PDO(12000,1500)
		,SNK_CAP_FIX_PDO(15000,1500)
    }
};



xbits8_t					dpm_config_flags[1];

#define PDINFO_USB_VENDOR_ID 0x29CF
#define PDINFO_USB_PRODUCT_ID 0x1711
#define PDINFO_USB_MFRS_STRING "RICHTEK"

pd_manufacturer_info_t  pd_manu_info={{PDINFO_USB_VENDOR_ID&0xFF,PDINFO_USB_VENDOR_ID>>8} ,{PDINFO_USB_PRODUCT_ID&0xFF,PDINFO_USB_PRODUCT_ID>>8},PDINFO_USB_MFRS_STRING};
