#pragma once
#include "RCC.hpp"

namespace System
{
	/**
	 * @brief Sets up the system clocks.
	 */
	using SystemClock_t = Peripherals::SystemClock
	<
		Peripherals::RCCGeneral::Clocks::HSE, 
		Peripherals::RCCGeneral::PCLKDivision::HCLK_1, 
		Peripherals::RCCGeneral::HCLKDivision::SYSCLK_1, 
		Peripherals::RCCGeneral::PLLMultiply::x6, 
		Peripherals::RCCGeneral::PLLDivision::Div1
	>;
	/**
	 * @brief The frequency of the system clock
	 */
	constexpr std::uint64_t const SystemClock	= SystemClock_t::Frequency();
	/**
	 * @brief The frequency of the timers clock
	 */
	constexpr std::uint32_t const TimerClock	= SystemClock_t::Timer();
}