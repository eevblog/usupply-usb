#pragma once
#include "PinDefinitions.hpp"

namespace System
{
    /**
     * Initially configured for minimum power consumption:
     *  Low frequency switching configured.
     *  System power disabled.
     */
    Pins::PWR_EN		pwr_en 			{ IO::Mode::Output, IO::State::Low };
    Pins::RM_NBOOT_ISO 	rm_nboot_iso	{ IO::Mode::Output, IO::State::High };
    Pins::HF_ON			hf_on			{ IO::Mode::Output, IO::State::High };
    /**
     * 
     */
    
}