#pragma once
#include "Pin.hpp"

namespace System::Pins
{
	//
	// USB pins
	using USB_DP 		= IO::Pin<IO::Port::A, 10>;
	using USB_DN 		= IO::Pin<IO::Port::A, 11>;
	//
	// I2C pins
	using I2C_SDA 		= IO::Pin<IO::Port::A, 7>;
	using I2C_SCL 		= IO::Pin<IO::Port::A, 6>;
	//
	// SWD pins
	using SWDIO 		= IO::Pin<IO::Port::A, 13>;
	using SWCLK 		= IO::Pin<IO::Port::A, 14>;
	//
	// UART pins
	using UART_TX 		= IO::Pin<IO::Port::A, 2>;
	using UART_RX 		= IO::Pin<IO::Port::A, 3>;
	//
	// OSC pins
	using OSC_IN 		= IO::Pin<IO::Port::F, 0>;
	using OSC_OUT 		= IO::Pin<IO::Port::F, 1>;
	//
	// INT_N pins	
	using INT_N 		= IO::Pin<IO::Port::A, 5>;
	//
	// GPIO pins
	using PWR_EN		= IO::Pin<IO::Port::A, 0>;
	//
	// 
	static constexpr unsigned VBUS_ADC_Channel = 2;
	using VBUS_ADC		= IO::Pin<IO::Port::A, 1>;
	using HF_ON			= IO::Pin<IO::Port::A, 4>;
	using RM_NBOOT_ISO	= IO::Pin<IO::Port::B, 1>;
	//
	// 
	using ADC_IN1 = IO::Pin<IO::Port::A, 0>;
	using ADC_IN2 = IO::Pin<IO::Port::A, 1>;
	using ADC_IN3 = IO::Pin<IO::Port::A, 3>;
	using ADC_IN4 = IO::Pin<IO::Port::A, 4>;
}