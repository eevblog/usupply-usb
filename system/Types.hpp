#pragma once
#include "Clocks.hpp"
#include "SysTick.hpp"
#include "CriticalSection.hpp"
#include "Constants.hpp"
#include "RT1716.hpp"

namespace System
{
	using CriticalSection = Peripherals::CriticalSection;
	/**
	 * @brief Sets up the system tick clock.
	 */
	using SysTick_t = Peripherals::SysTickCommon;
	/**
	 */
	using I2C_SDA 	= System::Pins::I2C_SDA;
	using I2C_SCL 	= System::Pins::I2C_SCL;
	using INT_N 	= System::Pins::INT_N;
	using RT1716_I2C = IO::I2C<I2C_SDA, I2C_SCL>;
	using RT1716 	= Parts::RT1716::RT1716<INT_N, RT1716_I2C>;
}